load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "1.2.12"

def zlib():
    maybe(
        http_archive,
        name = "zlib",
        build_file = Label("//:third_party/zlib/zlib.BUILD"),
        sha256 = "d8688496ea40fb61787500e863cc63c9afcbc524468cedeb478068924eb54932",
        strip_prefix = "zlib-{version}".format(version = _VERSION),
        url = "https://github.com/madler/zlib/archive/v{version}.tar.gz".format(version = _VERSION),
    )
