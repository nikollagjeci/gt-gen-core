load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

VERSION = "3.7.0"

def open_simulation_interface():
    maybe(
        http_archive,
        name = "open_simulation_interface",
        build_file = Label("//:third_party/open_simulation_interface/open_simulation_interface.BUILD"),
        url = "https://github.com//OpenSimulationInterface/open-simulation-interface/archive/refs/tags/v{version}.tar.gz".format(version = VERSION),
        sha256 = "b88a826e6ecae9573f76ec3eecc883773dad4f4e4fce6eea6910ac1dcf2e0cd3",
        strip_prefix = "open-simulation-interface-{version}".format(version = VERSION),
        patch_cmds = [
            "set -o errexit",
            "set -o nounset",
            "set -o pipefail",
            "sed 's/@VERSION_MAJOR@/{}/;s/@VERSION_MINOR@/{}/;s/@VERSION_PATCH@/{}/' osi_version.proto.in > osi_version.proto".format(*VERSION.split("."))
        ],
    )
