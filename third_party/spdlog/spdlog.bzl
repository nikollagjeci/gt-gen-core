load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "1.11.0"

def spdlog():
    maybe(
        http_archive,
        name = "spdlog",
        sha256 = "ca5cae8d6cac15dae0ec63b21d6ad3530070650f68076f3a4a862ca293a858bb",
        build_file = Label("//:third_party/spdlog/spdlog.BUILD"),
        strip_prefix = "spdlog-{v}".format(v = _VERSION),
        url =  "https://github.com/gabime/spdlog/archive/refs/tags/v{v}.tar.gz".format(v = _VERSION),
    )
