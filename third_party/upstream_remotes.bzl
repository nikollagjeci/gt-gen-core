""" Variables for third party upstream URLs
"""

GITHUB = "https://github.com"
ECLIPSE_GITLAB = "https://gitlab.eclipse.org/eclipse"
