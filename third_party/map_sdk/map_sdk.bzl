load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v0.3.0"

def map_sdk():
    maybe(
        http_archive,
        name = "map_sdk",
        url = ECLIPSE_GITLAB + "/openpass/map-sdk/-/archive/{tag}/map-sdk-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "ee67d9244ce3379f88034c1dd91543dd689812341b2bc6ee33c9d49b69084eaa",
        strip_prefix = "map-sdk-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
