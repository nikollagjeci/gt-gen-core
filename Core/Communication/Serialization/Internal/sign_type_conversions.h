/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_SIGNTYPECONVERSIONS_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_SIGNTYPECONVERSIONS_H

#include "Core/Environment/Map/GtGenMap/signs.h"
#include "gtgen_map.pb.h"

namespace gtgen::core::communication
{

inline messages::map::RoadMarkingType GtGenToProtoRoadMarkingType(osi::OsiRoadMarkingsType gtgen_marking_type)
{
    using gtgen_type = osi::OsiRoadMarkingsType;
    using proto_type = messages::map::RoadMarkingType;

    switch (gtgen_marking_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::ROAD_MARKING_TYPE_OTHER;
        }
        case gtgen_type::kPaintedTrafficSign:
        {
            return proto_type::ROAD_MARKING_TYPE_PAINTEDTRAFFICSIGN;
        }
        case gtgen_type::kSymbolicTrafficSign:
        {
            return proto_type::ROAD_MARKING_TYPE_SYMBOLICTRAFFICSIGN;
        }
        case gtgen_type::kTextualTrafficSign:
        {
            return proto_type::ROAD_MARKING_TYPE_TEXTUALTRAFFICSIGN;
        }
        case gtgen_type::kGenericSymbol:
        {
            return proto_type::ROAD_MARKING_TYPE_GENERICSYMBOL;
        }
        case gtgen_type::kGenericLine:
        {
            return proto_type::ROAD_MARKING_TYPE_GENERICLINE;
        }
        case gtgen_type::kGenericText:
        {
            return proto_type::ROAD_MARKING_TYPE_GENERICTEXT;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::ROAD_MARKING_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::RoadMarkingColor GtGenToProtoRoadMarkingColor(osi::OsiRoadMarkingColor gtgen_marking_color)
{
    using gtgen_type = osi::OsiRoadMarkingColor;
    using proto_type = messages::map::RoadMarkingColor;

    switch (gtgen_marking_color)
    {
        case gtgen_type::kOther:
        {
            return proto_type::ROAD_MARKING_COLOR_OTHER;
        }
        case gtgen_type::kWhite:
        {
            return proto_type::ROAD_MARKING_COLOR_WHITE;
        }
        case gtgen_type::kYellow:
        {
            return proto_type::ROAD_MARKING_COLOR_YELLOW;
        }
        case gtgen_type::kBlue:
        {
            return proto_type::ROAD_MARKING_COLOR_BLUE;
        }
        case gtgen_type::kRed:
        {
            return proto_type::ROAD_MARKING_COLOR_RED;
        }
        case gtgen_type::kGreen:
        {
            return proto_type::ROAD_MARKING_COLOR_GREEN;
        }
        case gtgen_type::kViolet:
        {
            return proto_type::ROAD_MARKING_COLOR_VIOLET;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::ROAD_MARKING_COLOR_UNKNOWN;
        }
    }
}

inline messages::map::SignVariability GtGenToProtoSignVariability(osi::OsiTrafficSignVariability gtgen_variability)
{
    using gtgen_type = osi::OsiTrafficSignVariability;
    using proto_type = messages::map::SignVariability;

    switch (gtgen_variability)
    {
        case gtgen_type::kOther:
        {
            return proto_type::SIGNVARIABILITY_OTHER;
        }
        case gtgen_type::kFixed:
        {
            return proto_type::SIGNVARIABILITY_FIXED;
        }
        case gtgen_type::kVariable:
        {
            return proto_type::SIGNVARIABILITY_VARIABLE;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::SIGNVARIABILITY_UNKNOWN;
        }
    }
}

inline messages::map::DirectionScope GtGenToProtoDirectionScope(osi::OsiTrafficSignDirectionScope gtgen_direction_scope)
{
    using gtgen_type = osi::OsiTrafficSignDirectionScope;
    using proto_type = messages::map::DirectionScope;

    switch (gtgen_direction_scope)
    {
        case gtgen_type::kOther:
        {
            return proto_type::DIRECTION_SCOPE_OTHER;
        }
        case gtgen_type::kNoDirection:
        {
            return proto_type::DIRECTION_SCOPE_NODIRECTION;
        }
        case gtgen_type::kLeft:
        {
            return proto_type::DIRECTION_SCOPE_LEFT;
        }
        case gtgen_type::kRight:
        {
            return proto_type::DIRECTION_SCOPE_RIGHT;
        }
        case gtgen_type::kLeftRight:
        {
            return proto_type::DIRECTION_SCOPE_LEFTRIGHT;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::DIRECTION_SCOPE_UNKNOWN;
        }
    }
}

inline messages::map::TrafficSignUnit GtGenToProtoSignUnit(osi::OsiTrafficSignValueUnit gtgen_unit)
{
    using gtgen_type = osi::OsiTrafficSignValueUnit;
    using proto_type = messages::map::TrafficSignUnit;

    switch (gtgen_unit)
    {
        case gtgen_type::kOther:
        {
            return proto_type::SIGN_UNIT_OTHER;
        }
        case gtgen_type::kNoUnit:
        {
            return proto_type::SIGN_UNIT_NOUNIT;
        }
        case gtgen_type::kKilometerPerHour:
        {
            return proto_type::SIGN_UNIT_KILOMETERPERHOUR;
        }
        case gtgen_type::kMilePerHour:
        {
            return proto_type::SIGN_UNIT_MILEPERHOUR;
        }
        case gtgen_type::kMeter:
        {
            return proto_type::SIGN_UNIT_METER;
        }
        case gtgen_type::kKilometer:
        {
            return proto_type::SIGN_UNIT_KILOMETER;
        }
        case gtgen_type::kFeet:
        {
            return proto_type::SIGN_UNIT_FEET;
        }
        case gtgen_type::kMile:
        {
            return proto_type::SIGN_UNIT_MILE;
        }
        case gtgen_type::kMetricTon:
        {
            return proto_type::SIGN_UNIT_METRICTON;
        }
        case gtgen_type::kLongTon:
        {
            return proto_type::SIGN_UNIT_LONGTON;
        }
        case gtgen_type::kShortTon:
        {
            return proto_type::SIGN_UNIT_SHORTTON;
        }
        case gtgen_type::kMinutes:
        {
            return proto_type::SIGN_UNIT_MINUTES;
        }
        case gtgen_type::kDay:
        {
            return proto_type::SIGN_UNIT_DAY;
        }
        case gtgen_type::kPercentage:
        {
            return proto_type::SIGN_UNIT_PERCENTAGE;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::SIGN_UNIT_UNKNOWN;
        }
    }
}

inline messages::map::MainTrafficSignType GtGenToProtoMainSign(osi::OsiTrafficSignType gtgen_sign_type)
{
    using gtgen_type = osi::OsiTrafficSignType;
    using proto_type = messages::map::MainTrafficSignType;

    switch (gtgen_sign_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::MAIN_SIGN_TYPE_OTHER;
        }
        case gtgen_type::kDangerSpot:
        {
            return proto_type::MAIN_SIGN_TYPE_DANGERSPOT;
        }
        case gtgen_type::kZebraCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_ZEBRACROSSING;
        }
        case gtgen_type::kFlight:
        {
            return proto_type::MAIN_SIGN_TYPE_FLIGHT;
        }
        case gtgen_type::kCattle:
        {
            return proto_type::MAIN_SIGN_TYPE_CATTLE;
        }
        case gtgen_type::kHorseRiders:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSERIDERS;
        }
        case gtgen_type::kAmphibians:
        {
            return proto_type::MAIN_SIGN_TYPE_AMPHIBIANS;
        }
        case gtgen_type::kFallingRocks:
        {
            return proto_type::MAIN_SIGN_TYPE_FALLINGROCKS;
        }
        case gtgen_type::kSnowOrIce:
        {
            return proto_type::MAIN_SIGN_TYPE_SNOWORICE;
        }
        case gtgen_type::kLooseGravel:
        {
            return proto_type::MAIN_SIGN_TYPE_LOOSEGRAVEL;
        }
        case gtgen_type::kWaterside:
        {
            return proto_type::MAIN_SIGN_TYPE_WATERSIDE;
        }
        case gtgen_type::kClearance:
        {
            return proto_type::MAIN_SIGN_TYPE_CLEARANCE;
        }
        case gtgen_type::kMovableBridge:
        {
            return proto_type::MAIN_SIGN_TYPE_MOVABLEBRIDGE;
        }
        case gtgen_type::kRightBeforeLeftNextIntersection:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTBEFORELEFTNEXTINTERSECTION;
        }
        case gtgen_type::kTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_TURNLEFT;
        }
        case gtgen_type::kTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_TURNRIGHT;
        }
        case gtgen_type::kDoubleTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DOUBLETURNLEFT;
        }
        case gtgen_type::kDoubleTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DOUBLETURNRIGHT;
        }
        case gtgen_type::kHillDownwards:
        {
            return proto_type::MAIN_SIGN_TYPE_HILLDOWNWARDS;
        }
        case gtgen_type::kHillUpwards:
        {
            return proto_type::MAIN_SIGN_TYPE_HILLUPWARDS;
        }
        case gtgen_type::kUnevenRoad:
        {
            return proto_type::MAIN_SIGN_TYPE_UNEVENROAD;
        }
        case gtgen_type::kRoadSlipperyWetOrDirty:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADSLIPPERYWETORDIRTY;
        }
        case gtgen_type::kSideWinds:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWINDS;
        }
        case gtgen_type::kRoadNarrowing:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADNARROWING;
        }
        case gtgen_type::kRoadNarrowingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADNARROWINGRIGHT;
        }
        case gtgen_type::kRoadNarrowingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADNARROWINGLEFT;
        }
        case gtgen_type::kRoadWorks:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADWORKS;
        }
        case gtgen_type::kTrafficQueues:
        {
            return proto_type::MAIN_SIGN_TYPE_TRAFFICQUEUES;
        }
        case gtgen_type::kTwoWayTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_TWOWAYTRAFFIC;
        }
        case gtgen_type::kAttentionTrafficLight:
        {
            return proto_type::MAIN_SIGN_TYPE_ATTENTIONTRAFFICLIGHT;
        }
        case gtgen_type::kPedestrians:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANS;
        }
        case gtgen_type::kChildrenCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_CHILDRENCROSSING;
        }
        case gtgen_type::kCycleRoute:
        {
            return proto_type::MAIN_SIGN_TYPE_CYCLEROUTE;
        }
        case gtgen_type::kDeerCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_DEERCROSSING;
        }
        case gtgen_type::kUngatedLevelCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_UNGATEDLEVELCROSSING;
        }
        case gtgen_type::kLevelCrossingMarker:
        {
            return proto_type::MAIN_SIGN_TYPE_LEVELCROSSINGMARKER;
        }
        case gtgen_type::kRailwayTrafficPriority:
        {
            return proto_type::MAIN_SIGN_TYPE_RAILWAYTRAFFICPRIORITY;
        }
        case gtgen_type::kGiveWay:
        {
            return proto_type::MAIN_SIGN_TYPE_GIVEWAY;
        }
        case gtgen_type::kStop:
        {
            return proto_type::MAIN_SIGN_TYPE_STOP;
        }
        case gtgen_type::kPriorityToOppositeDirection:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYTOOPPOSITEDIRECTION;
        }
        case gtgen_type::kPriorityToOppositeDirectionUpsideDown:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYTOOPPOSITEDIRECTIONUPSIDEDOWN;
        }
        case gtgen_type::kPrescribedLeftTurn:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURN;
        }
        case gtgen_type::kPrescribedRightTurn:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTTURN;
        }
        case gtgen_type::kPrescribedStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDSTRAIGHT;
        }
        case gtgen_type::kPrescribedRightWay:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTWAY;
        }
        case gtgen_type::kPrescribedLeftWay:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTWAY;
        }
        case gtgen_type::kPrescribedRightTurnAndStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTTURNANDSTRAIGHT;
        }
        case gtgen_type::kPrescribedLeftTurnAndStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURNANDSTRAIGHT;
        }
        case gtgen_type::kPrescribedLeftTurnAndRightTurn:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURNANDRIGHTTURN;
        }
        case gtgen_type::kPrescribedLeftTurnRightTurnAndStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURNRIGHTTURNANDSTRAIGHT;
        }
        case gtgen_type::kRoundabout:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUNDABOUT;
        }
        case gtgen_type::kOnewayLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_ONEWAYLEFT;
        }
        case gtgen_type::kOnewayRight:
        {
            return proto_type::MAIN_SIGN_TYPE_ONEWAYRIGHT;
        }
        case gtgen_type::kPassLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_PASSLEFT;
        }
        case gtgen_type::kPassRight:
        {
            return proto_type::MAIN_SIGN_TYPE_PASSRIGHT;
        }
        case gtgen_type::kSideLaneOpenForTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDELANEOPENFORTRAFFIC;
        }
        case gtgen_type::kSideLaneClosedForTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDELANECLOSEDFORTRAFFIC;
        }
        case gtgen_type::kSideLaneClosingForTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDELANECLOSINGFORTRAFFIC;
        }
        case gtgen_type::kBusStop:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSSTOP;
        }
        case gtgen_type::kTaxiStand:
        {
            return proto_type::MAIN_SIGN_TYPE_TAXISTAND;
        }
        case gtgen_type::kBicyclesOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESONLY;
        }
        case gtgen_type::kHorseRidersOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSERIDERSONLY;
        }
        case gtgen_type::kPedestriansOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANSONLY;
        }
        case gtgen_type::kBicyclesPedestriansSharedOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSHAREDONLY;
        }
        case gtgen_type::kBicyclesPedestriansSeparatedLeftOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSEPARATEDLEFTONLY;
        }
        case gtgen_type::kBicyclesPedestriansSeparatedRightOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSEPARATEDRIGHTONLY;
        }
        case gtgen_type::kPedestrianZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANZONEBEGIN;
        }
        case gtgen_type::kPedestrianZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANZONEEND;
        }
        case gtgen_type::kBicycleRoadBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLEROADBEGIN;
        }
        case gtgen_type::kBicycleRoadEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLEROADEND;
        }
        case gtgen_type::kBusLane:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSLANE;
        }
        case gtgen_type::kBusLaneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSLANEBEGIN;
        }
        case gtgen_type::kBusLaneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSLANEEND;
        }
        case gtgen_type::kAllProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_ALLPROHIBITED;
        }
        case gtgen_type::kMotorizedMultitrackProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOTORIZEDMULTITRACKPROHIBITED;
        }
        case gtgen_type::kTrucksProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_TRUCKSPROHIBITED;
        }
        case gtgen_type::kBicyclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPROHIBITED;
        }
        case gtgen_type::kMotorcyclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOTORCYCLESPROHIBITED;
        }
        case gtgen_type::kMopedsProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOPEDSPROHIBITED;
        }
        case gtgen_type::kHorseRidersProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSERIDERSPROHIBITED;
        }
        case gtgen_type::kHorseCarriagesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSECARRIAGESPROHIBITED;
        }
        case gtgen_type::kCattleProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_CATTLEPROHIBITED;
        }
        case gtgen_type::kBusesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSESPROHIBITED;
        }
        case gtgen_type::kCarsProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_CARSPROHIBITED;
        }
        case gtgen_type::kCarsTrailersProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_CARSTRAILERSPROHIBITED;
        }
        case gtgen_type::kTrucksTrailersProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_TRUCKSTRAILERSPROHIBITED;
        }
        case gtgen_type::kTractorsProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_TRACTORSPROHIBITED;
        }
        case gtgen_type::kPedestriansProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANSPROHIBITED;
        }
        case gtgen_type::kMotorVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOTORVEHICLESPROHIBITED;
        }
        case gtgen_type::kHazardousGoodsVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_HAZARDOUSGOODSVEHICLESPROHIBITED;
        }
        case gtgen_type::kOverWeightVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERWEIGHTVEHICLESPROHIBITED;
        }
        case gtgen_type::kVehiclesAxleOverWeightProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESAXLEOVERWEIGHTPROHIBITED;
        }
        case gtgen_type::kVehiclesExcessWidthProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESEXCESSWIDTHPROHIBITED;
        }
        case gtgen_type::kVehiclesExcessHeightProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESEXCESSHEIGHTPROHIBITED;
        }
        case gtgen_type::kVehiclesExcessLengthProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESEXCESSLENGTHPROHIBITED;
        }
        case gtgen_type::kDoNotEnter:
        {
            return proto_type::MAIN_SIGN_TYPE_DONOTENTER;
        }
        case gtgen_type::kSnowChainsRequired:
        {
            return proto_type::MAIN_SIGN_TYPE_SNOWCHAINSREQUIRED;
        }
        case gtgen_type::kWaterPollutantVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_WATERPOLLUTANTVEHICLESPROHIBITED;
        }
        case gtgen_type::kEnvironmentalZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ENVIRONMENTALZONEBEGIN;
        }
        case gtgen_type::kEnvironmentalZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ENVIRONMENTALZONEEND;
        }
        case gtgen_type::kNoUTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_NOUTURNLEFT;
        }
        case gtgen_type::kNoUTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_NOUTURNRIGHT;
        }
        case gtgen_type::kPrescribedUTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDUTURNLEFT;
        }
        case gtgen_type::kPrescribedUTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDUTURNRIGHT;
        }
        case gtgen_type::kMinimumDistanceForTrucks:
        {
            return proto_type::MAIN_SIGN_TYPE_MINIMUMDISTANCEFORTRUCKS;
        }
        case gtgen_type::kSpeedLimitBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITBEGIN;
        }
        case gtgen_type::kSpeedLimitZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITZONEBEGIN;
        }
        case gtgen_type::kSpeedLimitZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITZONEEND;
        }
        case gtgen_type::kMinimumSpeedBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_MINIMUMSPEEDBEGIN;
        }
        case gtgen_type::kOvertakingBanBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANBEGIN;
        }
        case gtgen_type::kOvertakingBanForTrucksBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANFORTRUCKSBEGIN;
        }
        case gtgen_type::kSpeedLimitEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITEND;
        }
        case gtgen_type::kMinimumSpeedEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_MINIMUMSPEEDEND;
        }
        case gtgen_type::kOvertakingBanEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANEND;
        }
        case gtgen_type::kOvertakingBanForTrucksEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANFORTRUCKSEND;
        }
        case gtgen_type::kAllRestrictionsEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ALLRESTRICTIONSEND;
        }
        case gtgen_type::kNoStopping:
        {
            return proto_type::MAIN_SIGN_TYPE_NOSTOPPING;
        }
        case gtgen_type::kNoParking:
        {
            return proto_type::MAIN_SIGN_TYPE_NOPARKING;
        }
        case gtgen_type::kNoParkingZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_NOPARKINGZONEBEGIN;
        }
        case gtgen_type::kNoParkingZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_NOPARKINGZONEEND;
        }
        case gtgen_type::kRightOfWayNextIntersection:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTOFWAYNEXTINTERSECTION;
        }
        case gtgen_type::kRightOfWayBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTOFWAYBEGIN;
        }
        case gtgen_type::kRightOfWayEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTOFWAYEND;
        }
        case gtgen_type::kPriorityOverOppositeDirection:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYOVEROPPOSITEDIRECTION;
        }
        case gtgen_type::kPriorityOverOppositeDirectionUpsideDown:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYOVEROPPOSITEDIRECTIONUPSIDEDOWN;
        }
        case gtgen_type::kTownBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_TOWNBEGIN;
        }
        case gtgen_type::kTownEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_TOWNEND;
        }
        case gtgen_type::kCarParking:
        {
            return proto_type::MAIN_SIGN_TYPE_CARPARKING;
        }
        case gtgen_type::kCarParkingZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_CARPARKINGZONEBEGIN;
        }
        case gtgen_type::kCarParkingZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_CARPARKINGZONEEND;
        }
        case gtgen_type::kSidewalkHalfParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKHALFPARKINGLEFT;
        }
        case gtgen_type::kSidewalkHalfParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKHALFPARKINGRIGHT;
        }
        case gtgen_type::kSidewalkParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPARKINGLEFT;
        }
        case gtgen_type::kSidewalkParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPARKINGRIGHT;
        }
        case gtgen_type::kSidewalkPerpendicularHalfParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARHALFPARKINGLEFT;
        }
        case gtgen_type::kSidewalkPerpendicularHalfParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARHALFPARKINGRIGHT;
        }
        case gtgen_type::kSidewalkPerpendicularParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARPARKINGLEFT;
        }
        case gtgen_type::kSidewalkPerpendicularParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARPARKINGRIGHT;
        }
        case gtgen_type::kLivingStreetBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_LIVINGSTREETBEGIN;
        }
        case gtgen_type::kLivingStreetEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_LIVINGSTREETEND;
        }
        case gtgen_type::kTunnel:
        {
            return proto_type::MAIN_SIGN_TYPE_TUNNEL;
        }
        case gtgen_type::kEmergencyStoppingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_EMERGENCYSTOPPINGLEFT;
        }
        case gtgen_type::kEmergencyStoppingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_EMERGENCYSTOPPINGRIGHT;
        }
        case gtgen_type::kHighwayBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYBEGIN;
        }
        case gtgen_type::kHighwayEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYEND;
        }
        case gtgen_type::kExpresswayBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_EXPRESSWAYBEGIN;
        }
        case gtgen_type::kExpresswayEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_EXPRESSWAYEND;
        }
        case gtgen_type::kNamedHighwayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_NAMEDHIGHWAYEXIT;
        }
        case gtgen_type::kNamedExpresswayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_NAMEDEXPRESSWAYEXIT;
        }
        case gtgen_type::kNamedRoadExit:
        {
            return proto_type::MAIN_SIGN_TYPE_NAMEDROADEXIT;
        }
        case gtgen_type::kHighwayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYEXIT;
        }
        case gtgen_type::kExpresswayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_EXPRESSWAYEXIT;
        }
        case gtgen_type::kOnewayStreet:
        {
            return proto_type::MAIN_SIGN_TYPE_ONEWAYSTREET;
        }
        case gtgen_type::kCrossingGuards:
        {
            return proto_type::MAIN_SIGN_TYPE_CROSSINGGUARDS;
        }
        case gtgen_type::kDeadend:
        {
            return proto_type::MAIN_SIGN_TYPE_DEADEND;
        }
        case gtgen_type::kDeadendExcludingDesignatedActors:
        {
            return proto_type::MAIN_SIGN_TYPE_DEADENDEXCLUDINGDESIGNATEDACTORS;
        }
        case gtgen_type::kFirstAidStation:
        {
            return proto_type::MAIN_SIGN_TYPE_FIRSTAIDSTATION;
        }
        case gtgen_type::kPoliceStation:
        {
            return proto_type::MAIN_SIGN_TYPE_POLICESTATION;
        }
        case gtgen_type::kTelephone:
        {
            return proto_type::MAIN_SIGN_TYPE_TELEPHONE;
        }
        case gtgen_type::kFillingStation:
        {
            return proto_type::MAIN_SIGN_TYPE_FILLINGSTATION;
        }
        case gtgen_type::kHotel:
        {
            return proto_type::MAIN_SIGN_TYPE_HOTEL;
        }
        case gtgen_type::kInn:
        {
            return proto_type::MAIN_SIGN_TYPE_INN;
        }
        case gtgen_type::kKiosk:
        {
            return proto_type::MAIN_SIGN_TYPE_KIOSK;
        }
        case gtgen_type::kToilet:
        {
            return proto_type::MAIN_SIGN_TYPE_TOILET;
        }
        case gtgen_type::kChapel:
        {
            return proto_type::MAIN_SIGN_TYPE_CHAPEL;
        }
        case gtgen_type::kTouristInfo:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTINFO;
        }
        case gtgen_type::kRepairService:
        {
            return proto_type::MAIN_SIGN_TYPE_REPAIRSERVICE;
        }
        case gtgen_type::kPedestrianUnderpass:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANUNDERPASS;
        }
        case gtgen_type::kPedestrianBridge:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANBRIDGE;
        }
        case gtgen_type::kCamperPlace:
        {
            return proto_type::MAIN_SIGN_TYPE_CAMPERPLACE;
        }
        case gtgen_type::kAdvisorySpeedLimitBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ADVISORYSPEEDLIMITBEGIN;
        }
        case gtgen_type::kAdvisorySpeedLimitEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ADVISORYSPEEDLIMITEND;
        }
        case gtgen_type::kPlaceName:
        {
            return proto_type::MAIN_SIGN_TYPE_PLACENAME;
        }
        case gtgen_type::kTouristAttraction:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTATTRACTION;
        }
        case gtgen_type::kTouristRoute:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTROUTE;
        }
        case gtgen_type::kTouristArea:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTAREA;
        }
        case gtgen_type::kShoulderNotPassableMotorVehicles:
        {
            return proto_type::MAIN_SIGN_TYPE_SHOULDERNOTPASSABLEMOTORVEHICLES;
        }
        case gtgen_type::kShoulderUnsafeTrucksTractors:
        {
            return proto_type::MAIN_SIGN_TYPE_SHOULDERUNSAFETRUCKSTRACTORS;
        }
        case gtgen_type::kTollBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_TOLLBEGIN;
        }
        case gtgen_type::kTollEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_TOLLEND;
        }
        case gtgen_type::kTollRoad:
        {
            return proto_type::MAIN_SIGN_TYPE_TOLLROAD;
        }
        case gtgen_type::kCustoms:
        {
            return proto_type::MAIN_SIGN_TYPE_CUSTOMS;
        }
        case gtgen_type::kInternationalBorderInfo:
        {
            return proto_type::MAIN_SIGN_TYPE_INTERNATIONALBORDERINFO;
        }
        case gtgen_type::kStreetlightRedBand:
        {
            return proto_type::MAIN_SIGN_TYPE_STREETLIGHTREDBAND;
        }
        case gtgen_type::kFederalHighwayRouteNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_FEDERALHIGHWAYROUTENUMBER;
        }
        case gtgen_type::kHighwayRouteNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYROUTENUMBER;
        }
        case gtgen_type::kHighwayInterchangeNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYINTERCHANGENUMBER;
        }
        case gtgen_type::kEuropeanRouteNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_EUROPEANROUTENUMBER;
        }
        case gtgen_type::kFederalHighwayDirectionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_FEDERALHIGHWAYDIRECTIONLEFT;
        }
        case gtgen_type::kFederalHighwayDirectionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_FEDERALHIGHWAYDIRECTIONRIGHT;
        }
        case gtgen_type::kPrimaryRoadDirectionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIMARYROADDIRECTIONLEFT;
        }
        case gtgen_type::kPrimaryRoadDirectionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIMARYROADDIRECTIONRIGHT;
        }
        case gtgen_type::kSecondaryRoadDirectionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SECONDARYROADDIRECTIONLEFT;
        }
        case gtgen_type::kSecondaryRoadDirectionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SECONDARYROADDIRECTIONRIGHT;
        }
        case gtgen_type::kDirectionDesignatedActorsLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONDESIGNATEDACTORSLEFT;
        }
        case gtgen_type::kDirectionDesignatedActorsRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONDESIGNATEDACTORSRIGHT;
        }
        case gtgen_type::kRoutingDesignatedActors:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUTINGDESIGNATEDACTORS;
        }
        case gtgen_type::kDirectionToHighwayLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOHIGHWAYLEFT;
        }
        case gtgen_type::kDirectionToHighwayRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOHIGHWAYRIGHT;
        }
        case gtgen_type::kDirectionToLocalDestinationLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOLOCALDESTINATIONLEFT;
        }
        case gtgen_type::kDirectionToLocalDestinationRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOLOCALDESTINATIONRIGHT;
        }
        case gtgen_type::kConsolidatedDirections:
        {
            return proto_type::MAIN_SIGN_TYPE_CONSOLIDATEDDIRECTIONS;
        }
        case gtgen_type::kStreetName:
        {
            return proto_type::MAIN_SIGN_TYPE_STREETNAME;
        }
        case gtgen_type::kDirectionPreannouncement:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENT;
        }
        case gtgen_type::kDirectionPreannouncementLaneConfig:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENTLANECONFIG;
        }
        case gtgen_type::kDirectionPreannouncementHighwayEntries:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENTHIGHWAYENTRIES;
        }
        case gtgen_type::kHighwayAnnouncement:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYANNOUNCEMENT;
        }
        case gtgen_type::kOtherRoadAnnouncement:
        {
            return proto_type::MAIN_SIGN_TYPE_OTHERROADANNOUNCEMENT;
        }
        case gtgen_type::kHighwayAnnouncementTruckStop:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYANNOUNCEMENTTRUCKSTOP;
        }
        case gtgen_type::kHighwayPreannouncementDirections:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYPREANNOUNCEMENTDIRECTIONS;
        }
        case gtgen_type::kPoleExit:
        {
            return proto_type::MAIN_SIGN_TYPE_POLEEXIT;
        }
        case gtgen_type::kHighwayDistanceBoard:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYDISTANCEBOARD;
        }
        case gtgen_type::kDetourLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURLEFT;
        }
        case gtgen_type::kDetourRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURRIGHT;
        }
        case gtgen_type::kNumberedDetour:
        {
            return proto_type::MAIN_SIGN_TYPE_NUMBEREDDETOUR;
        }
        case gtgen_type::kDetourBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURBEGIN;
        }
        case gtgen_type::kDetourEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOUREND;
        }
        case gtgen_type::kDetourRoutingBoard:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURROUTINGBOARD;
        }
        case gtgen_type::kOptionalDetour:
        {
            return proto_type::MAIN_SIGN_TYPE_OPTIONALDETOUR;
        }
        case gtgen_type::kOptionalDetourRouting:
        {
            return proto_type::MAIN_SIGN_TYPE_OPTIONALDETOURROUTING;
        }
        case gtgen_type::kRouteRecommendation:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUTERECOMMENDATION;
        }
        case gtgen_type::kRouteRecommendationEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUTERECOMMENDATIONEND;
        }
        case gtgen_type::kAnnounceLaneTransitionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELANETRANSITIONLEFT;
        }
        case gtgen_type::kAnnounceLaneTransitionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELANETRANSITIONRIGHT;
        }
        case gtgen_type::kAnnounceRightLaneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCERIGHTLANEEND;
        }
        case gtgen_type::kAnnounceLeftLaneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELEFTLANEEND;
        }
        case gtgen_type::kAnnounceRightLaneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCERIGHTLANEBEGIN;
        }
        case gtgen_type::kAnnounceLeftLaneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELEFTLANEBEGIN;
        }
        case gtgen_type::kAnnounceLaneConsolidation:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELANECONSOLIDATION;
        }
        case gtgen_type::kDetourCityBlock:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURCITYBLOCK;
        }
        case gtgen_type::kGate:
        {
            return proto_type::MAIN_SIGN_TYPE_GATE;
        }
        case gtgen_type::kPoleWarning:
        {
            return proto_type::MAIN_SIGN_TYPE_POLEWARNING;
        }
        case gtgen_type::kTrafficCone:
        {
            return proto_type::MAIN_SIGN_TYPE_TRAFFICCONE;
        }
        case gtgen_type::kMobileLaneClosure:
        {
            return proto_type::MAIN_SIGN_TYPE_MOBILELANECLOSURE;
        }
        case gtgen_type::kReflectorPost:
        {
            return proto_type::MAIN_SIGN_TYPE_REFLECTORPOST;
        }
        case gtgen_type::kDirectionalBoardWarning:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONALBOARDWARNING;
        }
        case gtgen_type::kGuidingPlate:
        {
            return proto_type::MAIN_SIGN_TYPE_GUIDINGPLATE;
        }
        case gtgen_type::kGuidingPlateWedges:
        {
            return proto_type::MAIN_SIGN_TYPE_GUIDINGPLATEWEDGES;
        }
        case gtgen_type::kParkingHazard:
        {
            return proto_type::MAIN_SIGN_TYPE_PARKINGHAZARD;
        }
        case gtgen_type::kTrafficLightGreenArrow:
        {
            return proto_type::MAIN_SIGN_TYPE_TRAFFICLIGHTGREENARROW;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::MAIN_SIGN_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::SupplementarySignType GtGenToProtoSupplementarySign(
    environment::map::OsiSupplementarySignType gtgen_sign_type)
{
    using gtgen_type = environment::map::OsiSupplementarySignType;
    using proto_type = messages::map::SupplementarySignType;

    switch (gtgen_sign_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::SUPPLEMENTARY_TYPE_OTHER;
        }
        case gtgen_type::kNoSign:
        {
            return proto_type::SUPPLEMENTARY_TYPE_NOSIGN;
        }
        case gtgen_type::kText:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TEXT;
        }
        case gtgen_type::kSpace:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SPACE;
        }
        case gtgen_type::kTime:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TIME;
        }
        case gtgen_type::kArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ARROW;
        }
        case gtgen_type::kConstrainedTo:
        {
            return proto_type::SUPPLEMENTARY_TYPE_CONSTRAINEDTO;
        }
        case gtgen_type::kExcept:
        {
            return proto_type::SUPPLEMENTARY_TYPE_EXCEPT;
        }
        case gtgen_type::kValidForDistance:
        {
            return proto_type::SUPPLEMENTARY_TYPE_VALIDFORDISTANCE;
        }
        case gtgen_type::kPriorityRoadBottomLeftFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTFOURWAY;
        }
        case gtgen_type::kPriorityRoadTopLeftFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPLEFTFOURWAY;
        }
        case gtgen_type::kPriorityRoadBottomLeftThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTTHREEWAYSTRAIGHT;
        }
        case gtgen_type::kPriorityRoadBottomLeftThreeWaySideways:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTTHREEWAYSIDEWAYS;
        }
        case gtgen_type::kPriorityRoadTopLeftThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPLEFTTHREEWAYSTRAIGHT;
        }
        case gtgen_type::kPriorityRoadBottomRightFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTFOURWAY;
        }
        case gtgen_type::kPriorityRoadTopRightFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPRIGHTFOURWAY;
        }
        case gtgen_type::kPriorityRoadBottomRightThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTTHREEWAYSTRAIGHT;
        }
        case gtgen_type::kPriorityRoadBottomRightThreeWaySideway:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTTHREEWAYSIDEWAY;
        }
        case gtgen_type::kPriorityRoadTopRightThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPRIGHTTHREEWAYSTRAIGHT;
        }
        case gtgen_type::kValidInDistance:
        {
            return proto_type::SUPPLEMENTARY_TYPE_VALIDINDISTANCE;
        }
        case gtgen_type::kStopIn:
        {
            return proto_type::SUPPLEMENTARY_TYPE_STOPIN;
        }
        case gtgen_type::kLeftArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_LEFTARROW;
        }
        case gtgen_type::kLeftBendArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_LEFTBENDARROW;
        }
        case gtgen_type::kRightArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_RIGHTARROW;
        }
        case gtgen_type::kRightBendArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_RIGHTBENDARROW;
        }
        case gtgen_type::kAccident:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ACCIDENT;
        }
        case gtgen_type::kSnow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SNOW;
        }
        case gtgen_type::kFog:
        {
            return proto_type::SUPPLEMENTARY_TYPE_FOG;
        }
        case gtgen_type::kRollingHighwayInformation:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ROLLINGHIGHWAYINFORMATION;
        }
        case gtgen_type::kServices:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SERVICES;
        }
        case gtgen_type::kTimeRange:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TIMERANGE;
        }
        case gtgen_type::kParkingDiscTimeRestriction:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PARKINGDISCTIMERESTRICTION;
        }
        case gtgen_type::kWeight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_WEIGHT;
        }
        case gtgen_type::kWet:
        {
            return proto_type::SUPPLEMENTARY_TYPE_WET;
        }
        case gtgen_type::kParkingConstraint:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PARKINGCONSTRAINT;
        }
        case gtgen_type::kNoWaitingSideStripes:
        {
            return proto_type::SUPPLEMENTARY_TYPE_NOWAITINGSIDESTRIPES;
        }
        case gtgen_type::kRain:
        {
            return proto_type::SUPPLEMENTARY_TYPE_RAIN;
        }
        case gtgen_type::kSnowRain:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SNOWRAIN;
        }
        case gtgen_type::kNight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_NIGHT;
        }
        case gtgen_type::kStop4Way:
        {
            return proto_type::SUPPLEMENTARY_TYPE_STOP4WAY;
        }
        case gtgen_type::kTruck:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TRUCK;
        }
        case gtgen_type::kTractorsMayBePassed:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TRACTORSMAYBEPASSED;
        }
        case gtgen_type::kHazardous:
        {
            return proto_type::SUPPLEMENTARY_TYPE_HAZARDOUS;
        }
        case gtgen_type::kTrailer:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TRAILER;
        }
        case gtgen_type::kZone:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ZONE;
        }
        case gtgen_type::kMotorcycle:
        {
            return proto_type::SUPPLEMENTARY_TYPE_MOTORCYCLE;
        }
        case gtgen_type::kMotorcycleAllowed:
        {
            return proto_type::SUPPLEMENTARY_TYPE_MOTORCYCLEALLOWED;
        }
        case gtgen_type::kCar:
        {
            return proto_type::SUPPLEMENTARY_TYPE_CAR;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::SUPPLEMENTARY_TYPE_UNKNOWN;
        }
    }
}

}  // namespace gtgen::core::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_SIGNTYPECONVERSIONS_H
