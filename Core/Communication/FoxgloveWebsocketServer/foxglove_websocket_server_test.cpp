/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/FoxgloveWebsocketServer/foxglove_websocket_server.h"

#include <arpa/inet.h>
#include <foxglove/websocket/websocket_client.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdint>

namespace gtgen::core::communication
{
void ConnectClient()
{
    const auto url = "ws://localhost:8765";
    std::unordered_map<foxglove::ChannelId, foxglove::SubscriptionId> sub_id_by_channel_id;
    foxglove::SubscriptionId next_sub_id = 0;
    foxglove::Client<foxglove::WebSocketNoTls> client;

    client.setBinaryMessageHandler([&](const uint8_t*, size_t) {});

    client.setTextMessageHandler([&](const std::string& payload) {
        const auto msg = nlohmann::json::parse(payload);
        const auto& op = msg["op"].get<std::string>();
        if (op != "advertise")
        {
            return;
        }

        const auto channels = msg["channels"].get<std::vector<foxglove::Channel>>();
        std::vector<std::pair<foxglove::SubscriptionId, foxglove::ChannelId>> subscribe_payload;

        for (const auto& channel : channels)
        {
            if (sub_id_by_channel_id.find(channel.id) == sub_id_by_channel_id.end())
            {
                const auto sub_id = next_sub_id++;
                subscribe_payload.push_back({sub_id, channel.id});
                sub_id_by_channel_id.insert({channel.id, sub_id});
            }
        }

        if (!subscribe_payload.empty())
        {
            client.subscribe(subscribe_payload);
        }
    });

    const auto open_handler = [&](websocketpp::connection_hdl) {};
    const auto close_handler = [&](websocketpp::connection_hdl) {};
    client.connect(url, open_handler, close_handler);
    // Wait until client thread has subscribed to channel with gt topic
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    client.close();
}

class FoxgloveWebsocketServerFixture : public ::testing::Test
{
  protected:
    bool IsServingAtPort(std::uint16_t port)
    {
        auto client_socket = socket(AF_INET, SOCK_STREAM, 0);
        EXPECT_GT(client_socket, 0);

        struct sockaddr_in serv_addr;
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(port);

        const std::string localhost{"127.0.0.1"};
        EXPECT_GT(inet_pton(AF_INET, localhost.c_str(), &serv_addr.sin_addr), 0);

        bool connected{connect(client_socket, reinterpret_cast<struct sockaddr*>(&serv_addr), sizeof(serv_addr)) == 0};
        close(client_socket);

        return connected;
    }

    FoxgloveWebsocketServer server_;
};

TEST_F(FoxgloveWebsocketServerFixture, GivenServerInstance_WhenNotStarted_ThenPort8765RefusesConnection)
{
    EXPECT_FALSE(IsServingAtPort(8765));
}

TEST_F(FoxgloveWebsocketServerFixture, GivenServerInstance_WhenStarted_ThenClientSubscribesToChannel)
{
    testing::internal::CaptureStdout();
    server_.Start();
    ConnectClient();

    const auto captured_stdout{testing::internal::GetCapturedStdout()};
    EXPECT_THAT(captured_stdout, testing::HasSubstr("subscribed to channel"));
}

}  // namespace gtgen::core::communication
