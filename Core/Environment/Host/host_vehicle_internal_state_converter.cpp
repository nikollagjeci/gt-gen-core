/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Host/host_vehicle_internal_state_converter.h"

#include "Core/Environment/Exception/exception.h"
#include "osi_hostvehicledata.pb.h"

#include <fmt/format.h>

namespace gtgen::core::environment::host
{

std::map<std::string, std::string> ConvertTrafficUpdateInternalStateToKeyValue(
    const osi3::TrafficUpdate& traffic_update)
{
    std::map<std::string, std::string> values;

    if (traffic_update.internal_state().size() > 1)
    {
        throw gtgen::core::environment::EnvironmentException("osi3::TrafficUpdate contains multiple internal states!");
    }
    if (traffic_update.internal_state().size() == 1)
    {
        const auto& host_vehicle_data = traffic_update.internal_state(0);
        for (const auto& vehicle_adf : host_vehicle_data.vehicle_automated_driving_function())
        {
            if (vehicle_adf.name() == osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_UNKNOWN)
            {
                continue;
            }
            const auto name =
                vehicle_adf.name() == osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_OTHER
                    ? vehicle_adf.custom_name()
                    : osi3::HostVehicleData::VehicleAutomatedDrivingFunction::Name_Name(vehicle_adf.name());

            if (vehicle_adf.state() != osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_UNKNOWN)
            {
                const auto state =
                    vehicle_adf.state() == osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_OTHER
                        ? vehicle_adf.custom_state()
                        : osi3::HostVehicleData::VehicleAutomatedDrivingFunction::State_Name(vehicle_adf.state());
                values[name] = state;
            }

            for (const auto& key_value : vehicle_adf.custom_detail())
            {
                const auto key = fmt::format("{}.{}", name, key_value.key());
                values[key] = key_value.value();
            }
        }
    }
    return values;
}
}  // namespace gtgen::core::environment::host
