/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/controller_factory.h"

#include "Core/Environment/Controller/Internal/ControlUnits/control_unit_test_utils.h"
#include "Core/Environment/Controller/Internal/composite_controller_sut.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "osi_groundtruth.pb.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::controller
{

TEST(ControllerFactoryTest, GivenNoOpConfig_WhenCreate_ThenNoOpControllerIsCreated)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config));

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(1, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::NoOpControlUnit>(controller->GetControlUnits()));
}

TEST(ControllerFactoryTest, GivenUnsupportedConfig_WhenCreate_ThenEnvironmentExceptionIsThrown)
{
    auto config = std::make_unique<mantle_api::IControllerConfig>();

    EXPECT_THROW(ControllerFactory::Create<CompositeControllerSUT>(0, std::move(config)), EnvironmentException);
}

TEST(ControllerFactoryTest, GivenSupportedConfig_WhenCreateWithoutRoutingBehavior_ThenDefaultRoutingBehaviorIsStop)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config));

    EXPECT_EQ(mantle_api::DefaultRoutingBehavior::kStop, controller->GetDefaultRoutingBehavior());
}

TEST(ControllerFactoryTest, GivenSupportedConfig_WhenCreateWithRoutingBehavior_ThenRoutingBehaviorIsSet)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(
        42, std::move(config), mantle_api::DefaultRoutingBehavior::kRandomRoute);

    EXPECT_EQ(mantle_api::DefaultRoutingBehavior::kRandomRoute, controller->GetDefaultRoutingBehavior());
}

TEST(ControllerFactoryTest, GivenExternalConfig_WhenCreate_ThenExternalCompositeControllerIsCreated)
{
    osi3::GroundTruth ground_truth;
    std::map<std::string, std::string> tpm_params{{"velocity", "22"}};

    auto config = std::make_unique<TrafficParticipantControlUnitConfig>();
    config->name = "traffic_participant_model_example";
    config->parameters = tpm_params;
    auto gtgen_map{test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    config->gtgen_map = gtgen_map.get();
    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config));

    EXPECT_EQ(controller->GetType(), CompositeController::Type::kExternal);
}

}  // namespace gtgen::core::environment::controller
