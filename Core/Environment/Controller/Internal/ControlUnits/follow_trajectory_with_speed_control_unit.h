/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYWITHSPEEDCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYWITHSPEEDCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Traffic/control_strategy.h>

#include <optional>

namespace gtgen::core::environment::controller
{

class FollowTrajectoryWithSpeedControlUnit : public IAbstractControlUnit
{
  public:
    /// @brief Create a follow trajectory with a pre-defined trajectory.
    /// @param trajectory Defines the trajectory to travel indicating at which time should be pass at each point
    /// @param time_reference Defines the time reference to be applied in the trajectory
    /// @throws EnvironmentException Throws when trajectory is empty.
    explicit FollowTrajectoryWithSpeedControlUnit(
        const mantle_api::Trajectory& trajectory,
        const mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference);

    FollowTrajectoryWithSpeedControlUnit(
        const FollowTrajectoryWithSpeedControlUnit& follow_trajectory_with_speed_control_unit);
    FollowTrajectoryWithSpeedControlUnit(FollowTrajectoryWithSpeedControlUnit&&) = default;
    FollowTrajectoryWithSpeedControlUnit& operator=(const FollowTrajectoryWithSpeedControlUnit&) = delete;
    FollowTrajectoryWithSpeedControlUnit& operator=(FollowTrajectoryWithSpeedControlUnit&&) = delete;
    ~FollowTrajectoryWithSpeedControlUnit() override = default;

    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    bool EntityNeedsToBeUpdated();
    void UpdateCurrentPosition(std::size_t lower_index, double ratio);
    void UpdateCurrentOrientation(std::size_t lower_index, double ratio);
    void UpdateCurrentVelocityAndAcceleration();
    std::optional<std::size_t> GetSimulationTimeIndexInPolyline();
    mantle_api::Time GetTimeInTrajectoryPointList(const std::size_t index) const;

    mantle_api::Trajectory trajectory_{};
    mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference_{};

    mantle_api::Vec3<units::velocity::meters_per_second_t> current_velocity_{};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> current_acceleration_{};
};

}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYWITHSPEEDCONTROLUNIT_H
