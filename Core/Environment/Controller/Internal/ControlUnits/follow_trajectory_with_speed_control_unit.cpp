/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_with_speed_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Service/Utility/derivative_utils.h"
#include "Core/Service/Utility/orientation_utils.h"

#include <MantleAPI/Common/floating_point_helper.h>

namespace gtgen::core::environment::controller
{
using units::literals::operator""_s;

FollowTrajectoryWithSpeedControlUnit::FollowTrajectoryWithSpeedControlUnit(
    const mantle_api::Trajectory& trajectory,
    const mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference)
    : trajectory_{trajectory}, time_reference_{time_reference}
{
    originating_control_strategy_ = mantle_api::ControlStrategyType::kFollowTrajectory;
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        if (poly_line.size() == 0)
        {
            throw EnvironmentException(
                "Trajectory with name {} doesn't contain any polyline points. Please adjust the scenario.",
                trajectory_.name);
        }
    }
}

FollowTrajectoryWithSpeedControlUnit::FollowTrajectoryWithSpeedControlUnit(
    const FollowTrajectoryWithSpeedControlUnit& follow_trajectory_with_speed_control_unit)
    : trajectory_{follow_trajectory_with_speed_control_unit.trajectory_},
      time_reference_{follow_trajectory_with_speed_control_unit.time_reference_}
{
    originating_control_strategy_ = follow_trajectory_with_speed_control_unit.originating_control_strategy_;
}

std::unique_ptr<IControlUnit> FollowTrajectoryWithSpeedControlUnit::Clone() const
{
    return std::make_unique<FollowTrajectoryWithSpeedControlUnit>(*this);
}

void FollowTrajectoryWithSpeedControlUnit::StepControlUnit()
{
    GTGEN_PROFILE_SCOPE
    if (EntityNeedsToBeUpdated())
    {
        if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
        {
            const auto lower_index = GetSimulationTimeIndexInPolyline().value();
            // Calculate ratio (0...1) between lower and upper polyline point
            const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
            auto ratio =
                lower_index < poly_line.size() - 1
                    ? (current_simulation_time_ - GetTimeInTrajectoryPointList(lower_index)) /
                          (GetTimeInTrajectoryPointList(lower_index + 1) - GetTimeInTrajectoryPointList(lower_index))
                    : units::dimensionless::scalar_t{0};

            UpdateCurrentPosition(lower_index, ratio);
            UpdateCurrentOrientation(lower_index, ratio);
            UpdateCurrentVelocityAndAcceleration();
        }
    }
}

bool FollowTrajectoryWithSpeedControlUnit::HasFinished() const
{
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        const auto last_point_time = GetTimeInTrajectoryPointList(poly_line.size() - 1);
        // control units are asked by scenario engine before they are stepped but after sim time has been ticked, so the
        // current_simulation_time_ is outdated
        return service::utility::Clock::Instance().Now() > last_point_time;
    }

    Warn(
        "FollowTrajectoryWithSpeedControlUnit: No other trajectory shapes than polyline supported. Finished "
        "controlling.");
    return true;
}

bool FollowTrajectoryWithSpeedControlUnit::EntityNeedsToBeUpdated()
{
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        const auto first_point_time = GetTimeInTrajectoryPointList(0);
        const auto last_point_time = GetTimeInTrajectoryPointList(poly_line.size() - 1);
        return current_simulation_time_ >= first_point_time && current_simulation_time_ <= last_point_time;
    }
    return false;
}

void FollowTrajectoryWithSpeedControlUnit::UpdateCurrentPosition(std::size_t lower_index, double ratio)
{
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        if (lower_index == poly_line.size() - 1)
        {
            entity_->SetPosition(poly_line[lower_index].pose.position);
        }
        else
        {
            auto pos1 = poly_line[lower_index].pose.position;
            auto pos2 = poly_line[lower_index + 1].pose.position;
            // interpolate position
            entity_->SetPosition(pos1 + ((pos2 - pos1) * ratio));
        }
    }
}

void FollowTrajectoryWithSpeedControlUnit::UpdateCurrentOrientation(std::size_t lower_index, double ratio)
{
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        const auto previous_point_orientation = poly_line[lower_index].pose.orientation;
        mantle_api::Orientation3<units::angle::radian_t> current_orientation{};

        if (lower_index == poly_line.size() - 1)
        {
            current_orientation = previous_point_orientation;
        }
        else
        {
            const auto next_point_orientation = poly_line[lower_index + 1].pose.orientation;
            const auto orientation_diff =
                service::utility::CalculateClampedDeltaOrientation(next_point_orientation, previous_point_orientation);
            const auto new_roll =
                service::utility::ClampOrientation(previous_point_orientation.roll + orientation_diff.roll * ratio);
            const auto new_yaw =
                service::utility::ClampOrientation(previous_point_orientation.yaw + orientation_diff.yaw * ratio);
            const auto new_pitch =
                service::utility::ClampOrientation(previous_point_orientation.pitch + orientation_diff.pitch * ratio);
            current_orientation = {new_yaw, new_pitch, new_roll};
        }
        SetEntityOrientationProperties(current_orientation);
    }
}

void FollowTrajectoryWithSpeedControlUnit::UpdateCurrentVelocityAndAcceleration()
{
    // TODO: Set velocity and acceleration as well in first step, when delta_time == 0_s
    // ==> find a way to set delta_time in first step, but this causes regression risk for all control units
    if (delta_time_ > 0.0_s)
    {
        entity_->SetVelocity(
            service::utility::GetPositionDerivativeVector<units::length::meter_t, units::velocity::meters_per_second_t>(
                entity_->GetPosition(), last_entity_position_, delta_time_));
        entity_->SetAcceleration(
            service::utility::GetPositionDerivativeVector<units::velocity::meters_per_second_t,
                                                          units::acceleration::meters_per_second_squared_t>(
                entity_->GetVelocity(), last_entity_velocity_, delta_time_));
    }
}

std::optional<std::size_t> FollowTrajectoryWithSpeedControlUnit::GetSimulationTimeIndexInPolyline()
{
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        std::size_t max_index = poly_line.size();

        for (std::size_t index = 0; index < poly_line.size(); index++)
        {
            const auto time_in_trajectory_point = GetTimeInTrajectoryPointList(index);
            if (current_simulation_time_ >= time_in_trajectory_point)
            {
                max_index = index;
            }
        }
        if (max_index < poly_line.size())
        {
            return max_index;
        }
    }
    return std::nullopt;
}

mantle_api::Time FollowTrajectoryWithSpeedControlUnit::GetTimeInTrajectoryPointList(const std::size_t index) const
{
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory_.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        if (!poly_line[index].time.has_value())
        {
            throw EnvironmentException(
                "Polyline point at index {} in trajectory with name {} doesn't contain time information. Please adjust "
                "the scenario.",
                index,
                trajectory_.name);
        }

        if (time_reference_.domainAbsoluteRelative == mantle_api::ReferenceContext::kAbsolute)
        {
            return (poly_line[index].time.value() * time_reference_.scale) + time_reference_.offset;
        }
        else
        {
            return spawn_time_ + (poly_line[index].time.value() * time_reference_.scale) + time_reference_.offset;
        }
    }
    return {};
}

}  // namespace gtgen::core::environment::controller
