/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit_base_fixture.h"
#include "Core/Environment/GtGenEnvironment/entity_repository.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"
#include "Core/Tests/TestUtils/expect_extensions.h"
#include "osi_groundtruth.pb.h"

#include <gmock/gmock.h>

namespace gtgen::core::environment::control_unit::test
{

namespace
{

using osi_traffic_participant::ITrafficParticipantModel;
using ::testing::_;
using ::testing::Return;
using ::testing::Truly;

class MockTrafficParticipantModel : public ITrafficParticipantModel
{
  public:
    MOCK_METHOD(osi_traffic_participant::TrafficParticipantUpdateResult,
                Update,
                (std::chrono::milliseconds delta_time,
                 const osi3::SensorView& sensor_view,
                 (const std::optional<osi3::TrafficCommand>& traffic_command)),
                (override));

    MOCK_METHOD((std::optional<osi3::SensorViewConfiguration>),
                GetSensorViewConfigurationRequest,
                (),
                (const override));

    MOCK_METHOD(void, SetSensorViewConfiguration, (osi3::SensorViewConfiguration sensor_view_config), (override));
};

class TrafficParticipantControlUnitTest : public TrafficParticipantControlUnitBaseFixture
{
  public:
    osi3::TrafficUpdate CreateTrafficUpdate(mantle_api::IEntity* entity)
    {
        osi3::TrafficUpdate traffic_update;
        auto* moving_object = traffic_update.add_update();

        moving_object->mutable_id()->set_value(entity->GetUniqueId());
        moving_object->set_type(osi3::MovingObject::TYPE_VEHICLE);
        moving_object->mutable_base()->mutable_position()->set_x(entity->GetPosition().x());
        moving_object->mutable_base()->mutable_position()->set_y(entity->GetPosition().y());
        moving_object->mutable_base()->mutable_position()->set_z(entity->GetPosition().z());

        moving_object->mutable_base()->mutable_velocity()->set_x(entity->GetVelocity().x());
        moving_object->mutable_base()->mutable_velocity()->set_y(entity->GetVelocity().y());
        moving_object->mutable_base()->mutable_velocity()->set_z(entity->GetVelocity().z());

        moving_object->mutable_base()->mutable_orientation()->set_yaw(entity->GetOrientation().yaw());
        moving_object->mutable_base()->mutable_orientation()->set_pitch(entity->GetOrientation().pitch());
        moving_object->mutable_base()->mutable_orientation()->set_roll(entity->GetOrientation().roll());

        moving_object->mutable_base()->mutable_orientation_rate()->set_yaw(entity->GetOrientationRate().yaw());
        moving_object->mutable_base()->mutable_orientation_rate()->set_pitch(entity->GetOrientationRate().pitch());
        moving_object->mutable_base()->mutable_orientation_rate()->set_roll(entity->GetOrientationRate().roll());

        moving_object->mutable_base()->mutable_orientation_acceleration()->set_yaw(
            entity->GetOrientationAcceleration().yaw());
        moving_object->mutable_base()->mutable_orientation_acceleration()->set_pitch(
            entity->GetOrientationAcceleration().pitch());
        moving_object->mutable_base()->mutable_orientation_acceleration()->set_roll(
            entity->GetOrientationAcceleration().roll());

        mantle_api::Dimension3& dimension = entity->GetProperties()->bounding_box.dimension;
        moving_object->mutable_base()->mutable_dimension()->set_length(dimension.length());
        moving_object->mutable_base()->mutable_dimension()->set_width(dimension.width());
        moving_object->mutable_base()->mutable_dimension()->set_height(dimension.height());

        auto* internal_state = traffic_update.add_internal_state();
        osi3::HostVehicleData host_vehicle_data;
        host_vehicle_data.mutable_host_vehicle_id()->set_value(entity->GetUniqueId());

        *internal_state = host_vehicle_data;

        return traffic_update;
    }

    std::shared_ptr<MockTrafficParticipantModel> mock_tpm_{
        std::make_shared<::testing::StrictMock<MockTrafficParticipantModel>>()};
    boost::function<osi_traffic_participant::Create> mock_tpm_creator_ =
        [&mock_tpm_ = mock_tpm_](uint64_t, const osi3::GroundTruth&, const std::map<std::string, std::string>&) {
            return mock_tpm_;
        };
};

TEST_F(TrafficParticipantControlUnitTest, GivenEntityNotInGroundTruth_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};

    EXPECT_FALSE(control_unit.HasFinished());
}

TEST_F(TrafficParticipantControlUnitTest, GivenTpmControlUnit_WhenClone_ThenCopyCreated)
{
    auto control_unit_ptr = std::make_unique<controller::TrafficParticipantControlUnit>(
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_);
    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenVehicleInGroundTruthAndEntityIsSet_WhenStepControlUnit_ThenNoThrownException)
{
    auto entity = CreateTpmEntity(1);
    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};

    control_unit.SetEntity(*entity);

    EXPECT_NO_THROW(control_unit.Step(mantle_api::Time{30}));
}

TEST_F(TrafficParticipantControlUnitTest, GivenVehicleInGroundTruth_WhenStepControlUnit_ThenHasNotFinished)
{
    auto entity = CreateTpmEntity(1);
    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};
    control_unit.SetEntity(*entity);

    control_unit.Step(mantle_api::Time{30});

    EXPECT_FALSE(control_unit.HasFinished());
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenEgoVehicleAtSimulationTimeZero_WhenStepControlUnit_ThenEntityIsNotChanged)
{
    auto ego_entity = CreateTpmEntity(1);
    ego_entity->SetVelocity({22_mps, 0_mps, 0_mps});
    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};
    control_unit.SetEntity(*ego_entity);

    control_unit.StepControlUnit();

    EXPECT_EQ(ego_entity->GetPosition().x(), 0);
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenEgoVehicleWithDesiredTargetVelocity_WhenStepControlUnit_ThenOnlyPositionIsUpdated)
{
    service::utility::UniqueIdProvider unique_id_provider{};
    api::EntityRepository entity_repository{&unique_id_provider};

    mantle_api::VehicleProperties vehicle_properties{};
    vehicle_properties.bounding_box.dimension = {2_m, 2_m, 1.5_m};
    vehicle_properties.is_host = true;
    const mantle_api::UniqueId kArbitraryEntityId{1U};
    auto& ego_entity = entity_repository.Create(kArbitraryEntityId, "TPM", vehicle_properties);
    ego_entity.SetAssignedLaneIds({3});
    ego_entity.SetVelocity({22_mps, 0_mps, 0_mps});

    controller::TrafficParticipantControlUnitConfig tpm_control_unit_config{};
    auto gtgen_map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    tpm_control_unit_config.gtgen_map = gtgen_map.get();
    tpm_control_unit_config.proto_ground_truth_builder_config.entity_repository = &entity_repository;
    tpm_control_unit_config.name = "traffic_participant_model_example";
    auto traffic_command_builder = std::make_unique<traffic_command::TrafficCommandBuilder>();
    tpm_control_unit_config.traffic_command_builder = traffic_command_builder.get();
    controller::TrafficParticipantControlUnit control_unit{tpm_params_, tpm_control_unit_config, mock_tpm_creator_};
    control_unit.SetEntity(ego_entity);

    osi_traffic_participant::TrafficParticipantUpdateResult mock_traffic_update_result{};
    const mantle_api::Vec3<units::length::meter_t> expected_position{
        units::length::meter_t(123.45),
        units::length::meter_t(ego_entity.GetPosition().y()),
        units::length::meter_t(ego_entity.GetPosition().z())};
    const mantle_api::Vec3<units::velocity::meters_per_second_t> expected_velocity{ego_entity.GetVelocity()};
    {
        mock_traffic_update_result.traffic_update = CreateTrafficUpdate(&ego_entity);
        auto* moving_object = mock_traffic_update_result.traffic_update.mutable_update(0);
        moving_object->mutable_base()->mutable_position()->set_x(expected_position.x.to<double>());
    }
    EXPECT_CALL(*mock_tpm_, Update(std::chrono::milliseconds{30}, _, _))
        .WillOnce(::testing::Return(mock_traffic_update_result));

    control_unit.Step(mantle_api::Time{0});  // Initial step will be always skipped
    control_unit.Step(mantle_api::Time{30});

    EXPECT_TRIPLE(expected_position, ego_entity.GetPosition());
    EXPECT_TRIPLE(expected_velocity, ego_entity.GetVelocity());
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenEgoVehicleWithEmptyCommand_WhenStepControlUnit_ThenTpmCalledWithEmptyCommand)
{
    constexpr mantle_api::UniqueId entity_id{1};
    auto entity = CreateTpmEntity(entity_id);

    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};
    control_unit.SetEntity(*entity);

    osi_traffic_participant::TrafficParticipantUpdateResult mock_traffic_update_result{};
    mock_traffic_update_result.traffic_update = CreateTrafficUpdate(entity);
    EXPECT_CALL(
        *mock_tpm_,
        Update(std::chrono::milliseconds{30}, _, Truly([](const std::optional<osi3::TrafficCommand>& traffic_command) {
                   return !traffic_command.has_value();
               })))
        .WillOnce(::testing::Return(mock_traffic_update_result));

    control_unit.Step(mantle_api::Time{0});  // Initial step will be always skipped
    control_unit.Step(mantle_api::Time{30});
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenEgoVehicleWithCommand_WhenStepControlUnit_ThenTpmCalledWithTheSameCommand)
{
    constexpr mantle_api::UniqueId entity_id{1};
    const std::string command_type{"command_type"};
    const std::string command{"command"};

    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};

    auto entity = CreateTpmEntity(entity_id);
    control_unit.SetEntity(*entity);

    // Set CustomAction and propagate it
    traffic_command_builder_->AddCustomTrafficActionForEntity(entity_id, command_type, command);
    traffic_command_builder_->Step();

    std::optional<osi3::TrafficCommand> actual_traffic_command{};
    osi_traffic_participant::TrafficParticipantUpdateResult mock_traffic_update_result{};
    mock_traffic_update_result.traffic_update = CreateTrafficUpdate(entity);
    EXPECT_CALL(
        *mock_tpm_,
        Update(std::chrono::milliseconds{30},
               _,
               Truly([&entity_id, &command_type, &command](const std::optional<osi3::TrafficCommand>& traffic_command) {
                   return traffic_command.has_value() &&
                          traffic_command->traffic_participant_id().value() == entity_id &&
                          traffic_command->action().size() && traffic_command->action(0).has_custom_action() &&
                          traffic_command->action(0).custom_action().command_type() == command_type &&
                          traffic_command->action(0).custom_action().command() == command;
               })))
        .WillOnce(::testing::Return(mock_traffic_update_result));

    control_unit.Step(mantle_api::Time{0});  // Initial step will be always skipped
    control_unit.Step(mantle_api::Time{30});
}

TEST_F(TrafficParticipantControlUnitTest, GivenEgoVehicle_WhenStepControlUnit_ThenUpdateSensorView)
{
    // Given
    const mantle_api::UniqueId kArbitraryEntityId{1U};
    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};
    auto entity = CreateTpmEntity(kArbitraryEntityId);
    control_unit.SetEntity(*entity);
    osi_traffic_participant::TrafficParticipantUpdateResult mock_traffic_update_result{};
    mock_traffic_update_result.traffic_update = CreateTrafficUpdate(entity);
    service::utility::Clock::Instance().SetNow(1500_ms);      // time since the start of the simulation
    const std::int64_t kExpectedSeconds = 1U;                 // seconds since the start of the simulation
    const std::uint32_t kExpectedNanoseconds = 500'000'000U;  // nanoseconds since the start of the last second

    // Expect
    EXPECT_CALL(*mock_tpm_,
                Update(std::chrono::milliseconds{30},
                       Truly([](const osi3::SensorView& sensor_view) {
                           return kExpectedSeconds == sensor_view.timestamp().seconds() &&
                                  kExpectedNanoseconds == sensor_view.timestamp().nanos();
                       }),
                       _))
        .WillOnce(::testing::Return(mock_traffic_update_result));

    // When
    control_unit.Step(mantle_api::Time{0});  // Initial step will be always skipped
    control_unit.Step(mantle_api::Time{30});
}

TEST_F(TrafficParticipantControlUnitTest, GivenTrafficUpdate_WhenStepControlUnit_ThenHostVehicleDataUpdate)
{
    // Given
    controller::TrafficParticipantControlUnit control_unit{
        tpm_params_, CreateTrafficParticipantControlConfig(), mock_tpm_creator_};
    auto entity = CreateTpmEntity(mantle_api::UniqueId{});
    control_unit.SetEntity(*entity);
    osi_traffic_participant::TrafficParticipantUpdateResult mock_traffic_update_result{};
    mock_traffic_update_result.traffic_update = CreateTrafficUpdate(entity);
    const auto internal_state = mock_traffic_update_result.traffic_update.internal_state(0);

    // Expect

    ::testing::Sequence seq;

    EXPECT_CALL(*mock_tpm_,
                Update(_,
                       Truly([&internal_state](const osi3::SensorView& sensor_view) {
                           return sensor_view.host_vehicle_data().DebugString() != internal_state.DebugString();
                       }),
                       _))
        .InSequence(seq)
        .WillOnce(::testing::Return(mock_traffic_update_result));

    EXPECT_CALL(*mock_tpm_,
                Update(_,
                       Truly([&internal_state](const osi3::SensorView& sensor_view) {
                           return sensor_view.host_vehicle_data().DebugString() == internal_state.DebugString();
                       }),
                       _))
        .InSequence(seq)
        .WillOnce(::testing::Return(mock_traffic_update_result));

    // When
    control_unit.Step(mantle_api::Time{0});  // Initial step will be always skipped
    control_unit.Step(mantle_api::Time{30});
    control_unit.Step(mantle_api::Time{60});
}

}  // namespace
}  // namespace gtgen::core::environment::control_unit::test
