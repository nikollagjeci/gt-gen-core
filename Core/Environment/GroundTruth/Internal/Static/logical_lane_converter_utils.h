/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LOGICAL_LANE_CONVERTER_UTILS_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LOGICAL_LANE_CONVERTER_UTILS_H

#include <MantleAPI/Common/i_identifiable.h>

#include <unordered_set>

namespace map_api
{
struct LogicalLane;
}  // namespace map_api

namespace osi3
{
class LogicalLane;
}  // namespace osi3

namespace gtgen::core::environment::proto_groundtruth
{
void FillLogicalLaneId(const map_api::LogicalLane* const gtgen_logical_lane,
                       osi3::LogicalLane* const proto_logical_lane);

void FillLogicalLaneType(const map_api::LogicalLane* const gtgen_logical_lane,
                         osi3::LogicalLane* const proto_logical_lane);

void FillLogicalLaneSourceReferences(const map_api::LogicalLane* const gtgen_logical_lane,
                                     osi3::LogicalLane* const proto_logical_lane);

void FillLogicalLanePhysicalLaneReferences(const map_api::LogicalLane* const gtgen_logical_lane,
                                           osi3::LogicalLane* const proto_logical_lane,
                                           const std::unordered_set<mantle_api::UniqueId>& existing_lane_ids);

void FillLogicalLaneReferenceLine(const map_api::LogicalLane* const gtgen_logical_lane,
                                  osi3::LogicalLane* const proto_logical_lane,
                                  const std::unordered_set<mantle_api::UniqueId>& existing_reference_line_ids);

void FillLogicalLaneRelationships(const map_api::LogicalLane* const gtgen_logical_lane,
                                  osi3::LogicalLane* const proto_logical_lane,
                                  const std::unordered_set<mantle_api::UniqueId>& existing_logical_lane_ids);

void FillLogicalLaneLogicalLaneBoundaries(const map_api::LogicalLane* const gtgen_logical_lane,
                                          osi3::LogicalLane* const proto_logical_lane);

void FillLogicalLaneConnections(const map_api::LogicalLane* const gtgen_logical_lane,
                                osi3::LogicalLane* const proto_logical_lane,
                                const std::unordered_set<mantle_api::UniqueId>& existing_logical_lane_ids);

void FillLogicalLaneStreetName(const map_api::LogicalLane* const gtgen_logical_lane,
                               osi3::LogicalLane* const proto_logical_lane);

}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LOGICAL_LANE_CONVERTER_UTILS_H
