/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Environment/environment_proto_ground_truth_builder.h"

#include "osi_environment.pb.h"

#include <MantleAPI/EnvironmentalConditions/weather.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::proto_groundtruth
{

class EnvironmentProtoGroundTruthBuilderTest : public testing::Test
{
  protected:
    mantle_api::Weather GetTestWeather()
    {
        mantle_api::Weather weather{};
        weather.precipitation = mantle_api::Precipitation::kVeryLight;
        weather.fog = mantle_api::Fog::kMist;
        weather.illumination = mantle_api::Illumination::kLevel5;
        weather.humidity = 75.2;
        weather.temperature = units::temperature::kelvin_t(23.7);
        weather.atmospheric_pressure = units::pressure::pascal_t{101'179.05};
        weather.sun.azimuth = units::angle::radian_t{0.47};
        weather.sun.elevation = units::angle::radian_t{1.17};
        weather.sun.intensity = units::illuminance::lux_t{12345.67};

        return weather;
    }
};

TEST_F(EnvironmentProtoGroundTruthBuilderTest, GivenWeatherTime_WhenFillEnvironment_ThenEnvironmentalConditionsFilled)
{
    EnvironmentProtoGroundTruthBuilder environment_proto_gt_builder(mantle_api::Time{0});
    osi3::EnvironmentalConditions proto_environment;
    mantle_api::Weather weather = GetTestWeather();
    mantle_api::Time time{60'000};

    environment_proto_gt_builder.SetWeather(weather);
    environment_proto_gt_builder.SetDateTime(time);
    environment_proto_gt_builder.FillEnvironment(&proto_environment);
    EXPECT_EQ(
        proto_environment.precipitation(),
        osi3::EnvironmentalConditions_Precipitation::EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_LIGHT);
    EXPECT_EQ(proto_environment.fog(), osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_MIST);
    EXPECT_EQ(proto_environment.ambient_illumination(),
              osi3::EnvironmentalConditions_AmbientIllumination::
                  EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL5);
    EXPECT_EQ(proto_environment.time_of_day().seconds_since_midnight(), 60);
    EXPECT_EQ(proto_environment.temperature(), 23.7);
    EXPECT_EQ(proto_environment.relative_humidity(), 75.2);
    EXPECT_EQ(proto_environment.atmospheric_pressure(), 101'179.05);

    ASSERT_TRUE(proto_environment.has_sun());
    EXPECT_DOUBLE_EQ(proto_environment.sun().azimuth(), 0.47);
    EXPECT_DOUBLE_EQ(proto_environment.sun().elevation(), 1.17);
    EXPECT_DOUBLE_EQ(proto_environment.sun().intensity(), 12345.67);
}

TEST_F(EnvironmentProtoGroundTruthBuilderTest,
       GivenDateTimeOneMinPastMidnightAndStepSizeOneMinute_WhenFillEnvironment_ThenTimeOfDayIsTwoMinutes)
{
    EnvironmentProtoGroundTruthBuilder environment_proto_gt_builder(mantle_api::Time{60'000});
    osi3::EnvironmentalConditions proto_environment;
    mantle_api::Time time{60'000};

    environment_proto_gt_builder.SetDateTime(time);
    environment_proto_gt_builder.FillEnvironment(&proto_environment);
    EXPECT_EQ(proto_environment.time_of_day().seconds_since_midnight(), 120);
}

}  // namespace gtgen::core::environment::proto_groundtruth
