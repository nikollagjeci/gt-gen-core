/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_BASICDATABUFFERIMPLEMENTATION_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_BASICDATABUFFERIMPLEMENTATION_H

#include "Core/Environment/DataStore/data_buffer_interface.h"

#include <cstddef>
#include <map>
#include <memory>
#include <optional>
#include <vector>

namespace gtgen::core::environment::datastore
{

/// @brief Container for DataBuffer cyclic values
using CyclicStore = std::vector<CyclicRow>;

/// @brief Container for indexs
using StoreIndex = std::multimap<mantle_api::UniqueId, std::size_t>;

/// @brief This class represents a set of cyclic data elements.
/// The result set of cyclic data elements refers to the elements in a databuffer.
///
class CyclicResult final : public CyclicResultInterface
{
  public:
    /// @brief CyclicResult constructor
    /// @param[in]   elements    The result set (referencing elements in a databuffer)
    CyclicResult(CyclicRowRefs elements);

    CyclicResult(const CyclicResult&) = delete;
    CyclicResult(CyclicResult&&) = delete;
    CyclicResult& operator=(const CyclicResult&) & = delete;
    CyclicResult& operator=(CyclicResult&&) & = delete;
    ~CyclicResult() final;

    std::size_t size() const override;
    const CyclicRow& at(const std::size_t index) const override;
    CyclicRowRefs::const_iterator begin() const override;
    CyclicRowRefs::const_iterator end() const override;

  private:
    const CyclicRowRefs elements;  //!< The result set (referencing elements in a databuffer)
};

/// @brief This class implements a basic version of a data buffer.
/// Data is stored in a simple mapping using agent ids as keys.
/// Values are stored as key/value pairs.
///
class BasicDataBufferImplementation : public DataBufferInterface
{
  public:
    BasicDataBufferImplementation();
    BasicDataBufferImplementation(const BasicDataBufferImplementation&) = delete;
    BasicDataBufferImplementation(BasicDataBufferImplementation&&) = delete;
    BasicDataBufferImplementation& operator=(const BasicDataBufferImplementation&) & = delete;
    BasicDataBufferImplementation& operator=(BasicDataBufferImplementation&&) & = delete;
    ~BasicDataBufferImplementation() override;

    /// @brief Store cyclic information related to the @c entity_id.
    ///
    /// @param[in] entity_id Id of the entity
    /// @param[in] key      Parameter key of the cyclic
    /// @param[in] value    Value of the cyclic to store
    void PutCyclic(const mantle_api::UniqueId entity_id, const Key& key, const Value& value) override;

    /// @brief Clears the data contents.
    void Clear() override;

    /// @brief Retrieve cyclic information related to the @c entity_id that matches the @p key, if @c entity_id is
    /// specified.
    ///        Otherwise retrieve cyclic information that simply matches the @p key.
    ///
    /// @param[in] entity_id Id of the entity
    /// @param[in] key Parameter key or the @c gtgen::core::environment::datastore::kWildcard that will match every key
    /// @return Pointer to the cyclic result interface
    std::unique_ptr<CyclicResultInterface> GetCyclic(const std::optional<mantle_api::UniqueId> entity_id,
                                                     const Key& key) const override;

  private:
    /// @brief Retrieve cyclic information related to the @c entity_id that matches the @p key.
    ///
    /// @param[in] entity_id Id of the entity
    /// @param[in] key Parameter key or the @c gtgen::core::environment::datastore::kWildcard that will match every key
    /// @return Pointer to the cyclic result interface
    std::unique_ptr<CyclicResultInterface> GetCyclic(const mantle_api::UniqueId entity_id, const Key& key) const;

    /// @brief Retrieve cyclic information that matches the @p key.
    ///
    /// @param[in] key Parameter key or the @c gtgen::core::environment::datastore::kWildcard that will match every key
    /// @return Pointer to the cyclic result interface
    std::unique_ptr<CyclicResultInterface> GetCyclic(const Key& key) const;

  protected:
    CyclicStore cyclic_store_;  //!< Container for DataBuffer cyclic values
};

}  // namespace gtgen::core::environment::datastore

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_BASICDATABUFFERIMPLEMENTATION_H
