/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_OUTPUTGENERATOR_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_OUTPUTGENERATOR_H

#include "Core/Environment/DataStore/cyclics.h"
#include "Core/Service/FileSystem/filesystem.h"

#include <cstdint>

namespace gtgen::core::environment::datastore
{

class DataBufferInterface;

class OutputGenerator
{
  public:
    /// @param[in] output_path Path to the directory where the output will be created.
    /// @param[in] data_buffer Data buffer from where the cyclics are retrieved each simulation step.
    OutputGenerator(const fs::path& output_path, DataBufferInterface* data_buffer);
    OutputGenerator(const OutputGenerator&) = delete;
    OutputGenerator(OutputGenerator&&) = delete;
    OutputGenerator& operator=(const OutputGenerator&) & = delete;
    OutputGenerator& operator=(OutputGenerator&&) & = delete;
    ~OutputGenerator();

    /// @brief Create the output directory, if does not exist.
    void Init();

    /// @brief Update the stored cyclics with the latest values from the buffer. Clear the buffer.
    /// @param[in] time Timestamp in milliseconds to which the cyclics refer.
    void Step(std::uint32_t time);

    /// @brief Create the output files, if data is present. Clear the data. Increment the run count.
    void FinishRun();

  private:
    /// @brief Update the stored cyclics with the latest values.
    /// @param[in] time Timestamp in milliseconds to which the cyclics refer.
    void StoreCyclics(std::uint32_t time);

    /// @brief Output the .csv file with the cyclics.
    void WriteCyclics();

    /// @brief The outputh directory path.
    fs::path output_path_;

    /// @brief Current run number.
    std::uint32_t run_number_;

    /// @brief Data buffer which contains the cyclics for the current simulation timestamp.
    DataBufferInterface* data_buffer_;

    /// @brief Container for the cyclics.
    Cyclics cyclics_;
};

}  // namespace gtgen::core::environment::datastore

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_OUTPUTGENERATOR_H
