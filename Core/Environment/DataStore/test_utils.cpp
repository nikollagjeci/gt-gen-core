/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/test_utils.h"

#include "Core/Service/Exception/exception.h"

namespace gtgen::core::environment::datastore::test_utils
{

std::vector<std::string> readCSV(const fs::path& output_file_path)
{
    std::ifstream file(output_file_path);
    std::string line;
    std::vector<std::string> result;

    if (file.is_open())
    {
        while (std::getline(file, line))
        {
            result.push_back(line);
        }
        file.close();
    }
    else
    {
        throw gtgen::core::service::ServiceException("Can't open file: ", output_file_path.c_str());
    }

    return result;
}

}  // namespace gtgen::core::environment::datastore::test_utils
