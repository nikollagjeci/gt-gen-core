/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/output_generator.h"

#include "Core/Environment/DataStore/basic_data_buffer_implementation.h"
#include "Core/Environment/DataStore/data_buffer_interface.h"
#include "Core/Environment/DataStore/utils.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Logging/logging.h"

#include <fmt/format.h>

#include <exception>
#include <fstream>
#include <optional>
#include <string>
#include <utility>
#include <variant>

namespace gtgen::core::environment::datastore
{
namespace
{
const std::string kCyclicsFilename = "Cyclics_Run_{:03}.csv";
}  // namespace

OutputGenerator::OutputGenerator(const fs::path& output_path, DataBufferInterface* data_buffer)
    : output_path_{output_path}, run_number_{0U}, data_buffer_{data_buffer}, cyclics_{}
{
}

OutputGenerator::~OutputGenerator() = default;

void OutputGenerator::Init()
{
    if (!output_path_.empty())
    {
        gtgen::core::service::file_system::CreateDirectoryIfNotExisting(output_path_);
    }
}

void OutputGenerator::Step(std::uint32_t time)
{
    StoreCyclics(time);
    data_buffer_->Clear();
}

void OutputGenerator::FinishRun()
{
    WriteCyclics();
    ++run_number_;
    cyclics_.Clear();
}

void OutputGenerator::StoreCyclics(std::uint32_t time)
{
    const auto ds_cyclics = data_buffer_->GetCyclic(std::nullopt, kWildcard);

    for (const CyclicRow& ds_cyclic : *ds_cyclics)
    {
        std::visit(utils::to_string([this, &ds_cyclic, &time](const std::string& value_str) {
                       cyclics_.Insert(time, fmt::format("{:02}:{}", ds_cyclic.entity_id, ds_cyclic.key), value_str);
                   }),
                   ds_cyclic.value);
    }
}

void OutputGenerator::WriteCyclics()
{
    try
    {
        if (!cyclics_.IsEmpty())
        {
            std::ofstream output_file;
            output_file.open(output_path_ / fmt::format(kCyclicsFilename, run_number_), std::ofstream::trunc);
            const auto& time_steps = cyclics_.GetTimeSteps();
            output_file << fmt::format("Timestep, {}\n", cyclics_.GetHeader());
            std::uint32_t time_step_number = 0;
            for (const auto time_step : time_steps)
            {
                output_file << fmt::format("{}, {}\n", time_step, cyclics_.GetSamplesLine(time_step_number));
                ++time_step_number;
            }
            output_file.flush();
            output_file.close();
        }
    }
    catch (const std::exception& ex)
    {
        Warn("Exception caught: {}", ex.what());
    }
}

}  // namespace gtgen::core::environment::datastore
