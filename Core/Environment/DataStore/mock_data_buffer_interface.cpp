/********************************************************************************
 * Copyright (c) 2020-2022 in-tech GmbH
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/mock_data_buffer_interface.h"

namespace gtgen::core::environment::datastore
{

}  // namespace gtgen::core::environment::datastore
