/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Core/Environment/DataStore/output_generator.h"

#include "Core/Environment/DataStore/mock_data_buffer_interface.h"
#include "Core/Environment/DataStore/test_utils.h"
#include "Core/Service/FileSystem/filesystem.h"

#include <fmt/format.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::datastore
{

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::StrictMock;

const std::string kCyclicsFilename = "Cyclics_Run_{:03}.csv";

class OutputGeneratorTest : public ::testing::Test
{
  protected:
    fs::path output_path = fs::temp_directory_path();
};

TEST_F(OutputGeneratorTest, WithEmptyCyclics_CyclicsOutputIsNotGenerated)
{
    // Given

    std::int32_t time = 100;
    std::unique_ptr<NiceMock<FakeCyclicResult>> empty_cyclic_result = std::make_unique<NiceMock<FakeCyclicResult>>();

    NiceMock<FakeDataBuffer> buffer;
    std::optional<mantle_api::UniqueId> entity_id = std::nullopt;
    ON_CALL(buffer, GetCyclic(entity_id, kWildcard)).WillByDefault(Return(ByMove(std::move(empty_cyclic_result))));

    OutputGenerator output_generator{output_path, &buffer};

    // When

    EXPECT_CALL(buffer, GetCyclic(entity_id, kWildcard)).Times(1);
    EXPECT_CALL(buffer, Clear()).Times(1);
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    // Expect

    ASSERT_FALSE(fs::exists(output_path / kCyclicsFilename))
        << "File exists even for empty cyclics: " << (output_path / kCyclicsFilename).string();
}

TEST_F(OutputGeneratorTest, WithCyclics_StoresCyclicsCorrectly)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const Key key{"key"};
    const Value value{1.0};
    const CyclicRow row{entity_id, key, value};
    const CyclicRowRefs row_refs{row};
    std::uint32_t time = 100U;
    std::uint32_t run_number = 0U;
    const fs::path output_file_path = output_path / fmt::format(kCyclicsFilename, run_number);
    const std::vector<std::string> expected_csv{fmt::format("Timestep, {}:{}", entity_id, key),
                                                fmt::format("{}, {}", time, std::to_string(std::get<double>(value)))};
    std::unique_ptr<NiceMock<FakeCyclicResult>> cyclic_result = std::make_unique<NiceMock<FakeCyclicResult>>();
    ON_CALL(*cyclic_result, begin()).WillByDefault(Return(row_refs.cbegin()));
    ON_CALL(*cyclic_result, end()).WillByDefault(Return(row_refs.cend()));

    StrictMock<FakeDataBuffer> buffer;
    std::optional<mantle_api::UniqueId> empty_entity_id = std::nullopt;

    OutputGenerator output_generator{output_path, &buffer};

    // When

    ASSERT_NO_THROW(output_generator.Init());
    EXPECT_CALL(buffer, GetCyclic(empty_entity_id, kWildcard)).WillOnce(Return(ByMove(std::move(cyclic_result))));
    EXPECT_CALL(buffer, Clear()).Times(1);
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    // Expect

    ASSERT_TRUE(fs::exists(output_file_path)) << "File does not exist: " << (output_file_path).string();
    EXPECT_EQ(expected_csv, test_utils::readCSV(output_file_path));
    fs::remove(output_file_path);
}

TEST_F(OutputGeneratorTest, WithMoreRuns_OutputMoreFiles)
{
    // Given

    const mantle_api::UniqueId entity_id{1'000U};
    const Key key{"key"};
    const Value value{1.0};
    const CyclicRow row{entity_id, key, value};
    const CyclicRowRefs row_refs{row};
    std::uint32_t time = 100U;
    std::uint32_t run_0 = 0U;
    std::uint32_t run_1 = 1U;
    const fs::path expected_output_file_path_0 = output_path / fmt::format(kCyclicsFilename, run_0);
    const fs::path expected_output_file_path_1 = output_path / fmt::format(kCyclicsFilename, run_1);
    const std::vector<std::string> expected_csv_0{fmt::format("Timestep, {}:{}", entity_id, key),
                                                  fmt::format("{}, {}", time, std::to_string(std::get<double>(value)))};
    const std::vector<std::string> expected_csv_1{fmt::format("Timestep, {}:{}", entity_id, key),
                                                  fmt::format("{}, {}", time, std::to_string(std::get<double>(value)))};
    std::unique_ptr<NiceMock<FakeCyclicResult>> cyclic_result_0 = std::make_unique<NiceMock<FakeCyclicResult>>();
    ON_CALL(*cyclic_result_0, begin()).WillByDefault(Return(row_refs.cbegin()));
    ON_CALL(*cyclic_result_0, end()).WillByDefault(Return(row_refs.cend()));

    std::unique_ptr<NiceMock<FakeCyclicResult>> cyclic_result_1 = std::make_unique<NiceMock<FakeCyclicResult>>();
    ON_CALL(*cyclic_result_1, begin()).WillByDefault(Return(row_refs.cbegin()));
    ON_CALL(*cyclic_result_1, end()).WillByDefault(Return(row_refs.cend()));

    StrictMock<FakeDataBuffer> buffer;
    std::optional<mantle_api::UniqueId> empty_entity_id = std::nullopt;

    OutputGenerator output_generator{output_path, &buffer};

    // When

    ASSERT_NO_THROW(output_generator.Init());
    EXPECT_CALL(buffer, GetCyclic(empty_entity_id, kWildcard)).WillOnce(Return(ByMove(std::move(cyclic_result_0))));
    EXPECT_CALL(buffer, Clear()).Times(1);
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    ASSERT_NO_THROW(output_generator.Init());
    EXPECT_CALL(buffer, GetCyclic(empty_entity_id, kWildcard)).WillOnce(Return(ByMove(std::move(cyclic_result_1))));
    EXPECT_CALL(buffer, Clear()).Times(1);
    ASSERT_NO_THROW(output_generator.Step(time));
    ASSERT_NO_THROW(output_generator.FinishRun());

    // Expect

    ASSERT_TRUE(fs::exists(expected_output_file_path_0))
        << "File does not exist: " << (expected_output_file_path_0).string();
    EXPECT_EQ(expected_csv_0, test_utils::readCSV(expected_output_file_path_0));
    fs::remove(expected_output_file_path_0);

    ASSERT_TRUE(fs::exists(expected_output_file_path_1))
        << "File does not exist: " << (expected_output_file_path_1).string();
    EXPECT_EQ(expected_csv_1, test_utils::readCSV(expected_output_file_path_1));
    fs::remove(expected_output_file_path_1);
}

}  // namespace gtgen::core::environment::datastore
