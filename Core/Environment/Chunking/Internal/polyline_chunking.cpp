/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Chunking/Internal/polyline_chunking.h"

#include "Core/Environment/Chunking/Internal/boost_gtgen_conversion.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"
#include "Core/Service/Utility/algorithm_utils.h"

#include <algorithm>

namespace gtgen::core::environment::chunking
{

namespace polyline_chunking
{
/// @brief Iterate over a box spanned by (and including) two chunks and apply a function on each chunk.
///
/// @tparam UnaryFunction Function that works on a chunk, has to be able to be called with a ChunkKey
/// @param chunk_a One "corner" chunk
/// @param chunk_b Other "corner" chunk
/// @param function Function to apply on each chunk, see description of UnaryFunction
template <typename UnaryFunction>
void ApplyToEncompassedChunks(const ChunkKey& chunk_a, const ChunkKey& chunk_b, UnaryFunction function)
{
    // Make sure both keys are sorted so that the box is walked through in ascending order,
    // from lower left to upper right, row-major

    ChunkKey lower_left;
    ChunkKey upper_right;
    lower_left.i = std::min(chunk_a.i, chunk_b.i);
    lower_left.j = std::min(chunk_a.j, chunk_b.j);
    upper_right.i = std::max(chunk_a.i, chunk_b.i);
    upper_right.j = std::max(chunk_a.j, chunk_b.j);

    for (std::int32_t i{lower_left.i}; i <= upper_right.i; ++i)
    {
        for (std::int32_t j{lower_left.j}; j <= upper_right.j; ++j)
        {
            function(ChunkKey{i, j});
        }
    }
}

}  // namespace polyline_chunking

namespace bg = boost::geometry;

void PolylineChunking::SetChunkSize(std::uint16_t chunk_size)
{
    if (chunk_size == 0)
    {
        throw EnvironmentException(
            "Chunk size must be greater than zero, "
            "please check the ChunkGridSize value of your User Settings and ensure that this contains a valid value.");
    }
    chunk_size_ = chunk_size;
}

void PolylineChunking::SetCellsPerDirection(std::uint16_t cells)
{
    cells_per_direction_ = cells;
}

std::vector<ChunkKey> PolylineChunking::GetChunkKeysAroundPoint(const CoordinateType& point) const
{
    std::vector<ChunkKey> result;
    std::size_t rank{2 * static_cast<std::size_t>(cells_per_direction_) + 1};
    result.reserve(rank * rank);

    const ChunkKey center_chunk_key{GetChunkKey(point)};

    for (std::int32_t i{-cells_per_direction_}; i <= cells_per_direction_; ++i)
    {
        for (std::int32_t j{-cells_per_direction_}; j <= cells_per_direction_; ++j)
        {
            result.emplace_back(ChunkKey{center_chunk_key.i + i, center_chunk_key.j + j});
        }
    }

    return result;
}

ChunkKey PolylineChunking::GetChunkKey(const CoordinateType& position) const
{
    // use std::floor to put e.g. -55 into chunk at position -100 (i.e. i = -2 instead of -1) with chunk size of 50
    return {static_cast<std::int32_t>(std::floor((position.x / chunk_size_)())),
            static_cast<std::int32_t>(std::floor((position.y / chunk_size_)()))};
}

void PolylineChunking::ValidateStaticChunkList(const StaticChunkList& static_chunk_list) const
{
    if (static_chunk_list.empty())
    {
        throw EnvironmentException("PolylineChunking: static chunk list is empty");
    }
    for (auto const& map_chunk : static_chunk_list)
    {
        map_chunk.second.Validate();
    }
}

CoordinateType PolylineChunking::GetLowerLeft(const ChunkKey& key) const
{
    return {units::length::meter_t(static_cast<double>(key.i) * chunk_size_),
            units::length::meter_t(static_cast<double>(key.j) * chunk_size_),
            units::length::meter_t(0)};
}

CoordinateType PolylineChunking::GetUpperRight(const ChunkKey& key) const
{
    return GetLowerLeft({key.i + 1, key.j + 1});
}

StaticChunkList PolylineChunking::InitializeChunks(const environment::map::GtGenMap& map)
{
    StaticChunkList chunks;
    ChunkLaneGroups(map, chunks);
    ChunkRoadObjects(map.road_objects, chunks);
    ChunkTrafficSigns(map.traffic_signs, chunks);
    ChunkTrafficLights(map.traffic_lights, chunks);
    ChunkReferenceLines(map.GetAll<map_api::ReferenceLine>(), chunks);
    ChunkLogicalLaneBoundaries(map.GetAll<map_api::LogicalLaneBoundary>(), chunks);
    ChunkLogicalLanes(map.GetAll<map_api::LogicalLane>(), chunks);
    ValidateStaticChunkList(chunks);

    return chunks;
}

void PolylineChunking::ChunkLaneGroups(const environment::map::GtGenMap& map, StaticChunkList& chunks)
{
    for (const environment::map::LaneGroup& lane_group : map.GetLaneGroups())
    {
        std::set<ChunkKey> keys_for_lanegroup;
        auto add_unless_added = [this, &keys_for_lanegroup, &lane_group, &chunks](const ChunkKey& key) {
            if (keys_for_lanegroup.emplace(key).second)
            {
                // key didn't exist yet --> need to add the lane-group to the chunk
                AddToChunk(key, &lane_group, chunks);
            }
        };

        // just a precaution to cover if a lane does not have lane-boundaries set
        for (const auto lane_id : lane_group.lane_ids)
        {
            const auto& lane = map.GetLane(lane_id);
            if (lane.left_lane_boundaries.empty() || lane.right_lane_boundaries.empty())
            {
                ProcessLineString(lane.center_line,
                                  add_unless_added,
                                  [](const mantle_api::Vec3<units::length::meter_t>& pos)
                                      -> const mantle_api::Vec3<units::length::meter_t>& { return pos; });
            }

            // process lane-boundaries in any case
            auto process_lane_boundary = [this, &map, &add_unless_added](const mantle_api::UniqueId& boundary_id) {
                const auto* boundary{map.FindLaneBoundary(boundary_id)};
                if (boundary != nullptr)
                {
                    ProcessLineString(
                        boundary->points,
                        add_unless_added,
                        [](const environment::map::LaneBoundary::Point& point)
                            -> const mantle_api::Vec3<units::length::meter_t>& { return point.position; });
                }
            };

            std::for_each(lane.left_lane_boundaries.begin(),
                          lane.left_lane_boundaries.end(),
                          [&process_lane_boundary](const auto& boundary_id) { process_lane_boundary(boundary_id); });

            std::for_each(lane.right_lane_boundaries.begin(),
                          lane.right_lane_boundaries.end(),
                          [&process_lane_boundary](const auto& boundary_id) { process_lane_boundary(boundary_id); });
        }
    }
}

void PolylineChunking::ChunkRoadObjects(const environment::map::RoadObjects& road_objects, StaticChunkList& chunks)
{
    for (const environment::map::RoadObject& road_object : road_objects)
    {
        std::set<ChunkKey> keys_for_road_object;
        auto add_unless_added = [this, &keys_for_road_object, &road_object, &chunks](const ChunkKey& key) {
            if (keys_for_road_object.emplace(key).second)
            {
                // key didn't exist yet --> need to add the road-object to the chunk
                AddToChunk(key, &road_object, chunks);
            }
        };

        add_unless_added(GetChunkKey(road_object.pose.position));
        ProcessLineString(road_object.base_polygon,
                          add_unless_added,
                          [&road_object](const mantle_api::Vec3<units::length::meter_t>& v) {
                              return road_object.pose.position + v;
                          });
    }
}

void PolylineChunking::ChunkTrafficSigns(const environment::map::TrafficSigns& traffic_signs, StaticChunkList& chunks)
{
    for (const std::shared_ptr<environment::map::TrafficSign>& traffic_sign : traffic_signs)
    {
        AddToChunk(GetChunkKey(traffic_sign->pose.position), traffic_sign.get(), chunks);
    }
}

void PolylineChunking::ChunkTrafficLights(const environment::map::TrafficLights& traffic_lights,
                                          StaticChunkList& chunks)
{
    for (const environment::map::TrafficLight& traffic_light : traffic_lights)
    {
        AddToChunk(GetChunkKey(traffic_light.light_bulbs.front().pose.position), &traffic_light, chunks);
    }
}

void PolylineChunking::ChunkReferenceLines(const std::vector<map_api::ReferenceLine>& reference_lines,
                                           StaticChunkList& chunks)
{
    for (const map_api::ReferenceLine& reference_line : reference_lines)
    {
        std::set<ChunkKey> keys_for_reference_line;
        auto add_unless_added = [this, &keys_for_reference_line, &reference_line, &chunks](const ChunkKey& key) {
            if (keys_for_reference_line.emplace(key).second)
            {
                // key didn't exist yet --> need to add the road-object to the chunk
                AddToChunk(key, &reference_line, chunks);
            }
        };
        ProcessLineString(reference_line.poly_line,
                          add_unless_added,
                          [](const map_api::ReferenceLinePoint& point) -> const map_api::Position3d& {
                              return point.world_position;
                          });
    }
}

void PolylineChunking::ChunkLogicalLaneBoundaries(
    const std::vector<map_api::LogicalLaneBoundary>& logical_lane_boundaries,
    StaticChunkList& chunks)
{
    for (const map_api::LogicalLaneBoundary& logical_lane_boundary : logical_lane_boundaries)
    {
        std::set<ChunkKey> keys_for_logical_lane_boundary;
        auto add_unless_added =
            [this, &keys_for_logical_lane_boundary, &logical_lane_boundary, &chunks](const ChunkKey& key) {
                if (keys_for_logical_lane_boundary.emplace(key).second)
                {
                    // key didn't exist yet --> need to add the road-object to the chunk
                    AddToChunk(key, &logical_lane_boundary, chunks);
                }
            };
        ProcessLineString(
            logical_lane_boundary.boundary_line,
            add_unless_added,
            [](const map_api::LogicalBoundaryPoint& point) -> const map_api::Position3d& { return point.position; });
    }
}

void PolylineChunking::ChunkLogicalLanes(const std::vector<map_api::LogicalLane>& logical_lanes,
                                         StaticChunkList& chunks)
{
    for (const map_api::LogicalLane& logical_lane : logical_lanes)
    {
        std::set<ChunkKey> keys_for_logical_lane;
        auto add_unless_added = [this, &keys_for_logical_lane, &logical_lane, &chunks](const ChunkKey& key) {
            if (keys_for_logical_lane.emplace(key).second)
            {
                // key didn't exist yet --> need to add the logical lane to the chunk
                AddToChunk(key, &logical_lane, chunks);
            }
        };
        auto process_logical_lane_boundary = [this, &add_unless_added](const auto& logical_lane_boundary) {
            ProcessLineString(logical_lane_boundary.boundary_line,
                              add_unless_added,
                              [](const map_api::LogicalBoundaryPoint& point) -> const map_api::Position3d& {
                                  return point.position;
                              });
        };
        std::for_each(logical_lane.left_boundaries.begin(),
                      logical_lane.left_boundaries.end(),
                      [&process_logical_lane_boundary](const auto& logical_lane_boundary) {
                          process_logical_lane_boundary(logical_lane_boundary.get());
                      });
        std::for_each(logical_lane.right_boundaries.begin(),
                      logical_lane.right_boundaries.end(),
                      [&process_logical_lane_boundary](const auto& logical_lane_boundary) {
                          process_logical_lane_boundary(logical_lane_boundary.get());
                      });
    }
}

MapChunk& PolylineChunking::GetOrCreateChunk(const ChunkKey& key, StaticChunkList& chunks) const
{
    return chunks.try_emplace(key, key, GetLowerLeft(key)).first->second;
}

void PolylineChunking::AddToChunk(const ChunkKey& key,
                                  const environment::map::LaneGroup* const lane_group,
                                  StaticChunkList& chunks)
{
    MapChunk& chunk{GetOrCreateChunk(key, chunks)};
    chunk.lane_groups.emplace_back(lane_group);
}

void PolylineChunking::AddToChunk(const ChunkKey& key,
                                  const environment::map::RoadObject* const road_object,
                                  StaticChunkList& chunks)
{
    MapChunk& chunk{GetOrCreateChunk(key, chunks)};
    chunk.road_objects.emplace_back(road_object);
}

void PolylineChunking::AddToChunk(const ChunkKey& key,
                                  const environment::map::TrafficSign* const traffic_sign,
                                  StaticChunkList& chunks)
{
    MapChunk& chunk{GetOrCreateChunk(key, chunks)};
    chunk.traffic_signs.emplace_back(traffic_sign);
}

void PolylineChunking::AddToChunk(const ChunkKey& key,
                                  const environment::map::TrafficLight* traffic_light,
                                  StaticChunkList& chunks)
{
    MapChunk& chunk{GetOrCreateChunk(key, chunks)};
    chunk.traffic_lights.emplace_back(traffic_light);
}

void PolylineChunking::AddToChunk(const ChunkKey& key,
                                  const map_api::ReferenceLine* reference_line,
                                  StaticChunkList& chunks)
{
    MapChunk& chunk{GetOrCreateChunk(key, chunks)};
    chunk.reference_lines.emplace_back(reference_line);
}

void PolylineChunking::AddToChunk(const ChunkKey& key,
                                  const map_api::LogicalLaneBoundary* logical_lane_boundary,
                                  StaticChunkList& chunks)
{
    MapChunk& chunk{GetOrCreateChunk(key, chunks)};
    chunk.logical_lane_boundaries.emplace_back(logical_lane_boundary);
}

void PolylineChunking::AddToChunk(const ChunkKey& key,
                                  const map_api::LogicalLane* logical_lane,
                                  StaticChunkList& chunks)
{
    MapChunk& chunk{GetOrCreateChunk(key, chunks)};
    chunk.logical_lanes.emplace_back(logical_lane);
}

template <typename ValueType, typename ChunkKeyProcessor, typename PositionAccessor>
void PolylineChunking::ProcessLineString(const std::vector<ValueType>& line_string,
                                         ChunkKeyProcessor process_chunk_key,
                                         PositionAccessor access_position)
{
    service::utility::ForAdjacentEach(
        line_string.begin(),
        line_string.end(),
        [this, &access_position, &process_chunk_key](const ValueType& segment_start, const ValueType& segment_end) {
            const CoordinateType& start_position{access_position(segment_start)};
            const CoordinateType& end_position{access_position(segment_end)};

            const ChunkKey key_a{GetChunkKey(start_position)};
            const ChunkKey key_b{GetChunkKey(end_position)};

            process_chunk_key(key_a);

            // check the rest of the segment, if the complete segment is not in the same chunk
            if (key_a != key_b)
            {
                polyline_chunking::ApplyToEncompassedChunks(
                    key_a, key_b, [this, &process_chunk_key, &start_position, &end_position](const ChunkKey& key) {
                        if (bg::intersects(
                                boost::geometry::model::box<glm::dvec3>{
                                    service::glmwrapper::ToGlmVec3(GetLowerLeft(key)),
                                    service::glmwrapper::ToGlmVec3(GetUpperRight(key))},
                                service::glmwrapper::ToVectorGlmVec3(
                                    std::vector<CoordinateType>{start_position, end_position})))
                        {
                            process_chunk_key(key);
                        }
                    });
            }
        });
}

WorldChunk PolylineChunking::ConvertToGroundTruthChunk(const MapChunk& chunk) const
{
    WorldChunk result;
    result.lower_left = chunk.lower_left;
    result.key = chunk.key;
    result.lane_groups = chunk.lane_groups;
    result.road_objects = chunk.road_objects;
    result.traffic_signs = chunk.traffic_signs;
    result.traffic_lights = chunk.traffic_lights;
    result.reference_lines = chunk.reference_lines;
    result.logical_lane_boundaries = chunk.logical_lane_boundaries;
    result.logical_lanes = chunk.logical_lanes;
    return result;
}

WorldChunk PolylineChunking::ConvertToGroundTruthChunk(MapChunk&& chunk) const
{
    WorldChunk result;
    result.lower_left = chunk.lower_left;
    result.key = chunk.key;
    result.lane_groups = std::move(chunk.lane_groups);
    result.road_objects = std::move(chunk.road_objects);
    result.traffic_signs = std::move(chunk.traffic_signs);
    result.traffic_lights = std::move(chunk.traffic_lights);
    result.reference_lines = std::move(chunk.reference_lines);
    result.logical_lane_boundaries = std::move(chunk.logical_lane_boundaries);
    result.logical_lanes = std::move(chunk.logical_lanes);
    return result;
}

}  // namespace gtgen::core::environment::chunking
