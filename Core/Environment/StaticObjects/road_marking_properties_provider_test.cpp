/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/road_marking_properties_provider.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::static_objects
{

TEST(RoadMarkingPropertiesProviderTest, GivenEmptyIdentifier_WhenGet_ThenNullptrReturned)
{
    auto properties = RoadMarkingPropertiesProvider::Get("");
    auto road_marking_properties = dynamic_cast<mantle_ext::RoadMarkingProperties*>(properties.get());
    ASSERT_TRUE(road_marking_properties == nullptr);
}

TEST(RoadMarkingPropertiesProviderTest, GivenAnyRoadMarkingIdentifier_WhenGet_ThenNullptrReturned)
{
    auto properties = RoadMarkingPropertiesProvider::Get("anything");
    auto road_marking_properties = dynamic_cast<mantle_ext::RoadMarkingProperties*>(properties.get());
    ASSERT_TRUE(road_marking_properties == nullptr);
}

TEST(RoadMarkingPropertiesProviderTest, GivenNonRoadMarkingIdentifier_WhenGet_ThenNullptrReturned)
{
    auto properties = RoadMarkingPropertiesProvider::Get("123456789");
    auto road_marking_properties = dynamic_cast<mantle_ext::RoadMarkingProperties*>(properties.get());
    ASSERT_TRUE(road_marking_properties == nullptr);
}

class GetStvoSignsAndRoadMarkingTypeTest
    : public testing::TestWithParam<std::tuple<std::string, osi::OsiTrafficSignType, osi::OsiRoadMarkingsType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    AllStvoSignsAndRoadMarkingTypes,
    GetStvoSignsAndRoadMarkingTypeTest,
    testing::Values(
        std::make_tuple("293", osi::OsiTrafficSignType::kZebraCrossing, osi::OsiRoadMarkingsType::kSymbolicTrafficSign),
        std::make_tuple("294", osi::OsiTrafficSignType::kStop, osi::OsiRoadMarkingsType::kSymbolicTrafficSign),
        std::make_tuple("299", osi::OsiTrafficSignType::kNoParking, osi::OsiRoadMarkingsType::kSymbolicTrafficSign),
        std::make_tuple("341", osi::OsiTrafficSignType::kGiveWay, osi::OsiRoadMarkingsType::kSymbolicTrafficSign)));

TEST_P(GetStvoSignsAndRoadMarkingTypeTest, GivenIdentifier_WhenGet_ThenCorrectTypes)
{
    std::string identifier = std::get<0>(GetParam());
    auto expected_sign_type = std::get<1>(GetParam());
    auto expected_road_marking_type = std::get<2>(GetParam());

    auto properties = RoadMarkingPropertiesProvider::Get(identifier);
    auto road_marking_properties = dynamic_cast<mantle_ext::RoadMarkingProperties*>(properties.get());
    ASSERT_TRUE(road_marking_properties != nullptr);

    EXPECT_EQ(expected_sign_type, road_marking_properties->sign_type);
    EXPECT_EQ(expected_road_marking_type, road_marking_properties->marking_type);
}

}  // namespace gtgen::core::environment::static_objects
