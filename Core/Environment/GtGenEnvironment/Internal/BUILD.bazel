cc_library(
    name = "active_controller_repository",
    srcs = ["active_controller_repository.cpp"],
    hdrs = ["active_controller_repository.h"],
    visibility = ["//Core/Environment/GtGenEnvironment:__subpackages__"],
    deps = [
        "//Core/Environment/Controller:composite_controller",
        "//Core/Service/Logging:logging",
    ],
)

cc_test(
    name = "active_controller_repository_test",
    timeout = "short",
    srcs = ["active_controller_repository_test.cpp"],
    deps = [
        ":active_controller_repository",
        ":controller_prototypes",
        "//Core/Environment/GtGenEnvironment:entity_repository",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "controller_prototypes",
    srcs = ["controller_prototypes.cpp"],
    hdrs = ["controller_prototypes.h"],
    visibility = ["//Core/Environment/GtGenEnvironment:__subpackages__"],
    deps = [
        "//Core/Environment/Controller:controller_factory",
        "//Core/Environment/Controller:external_controller_config_converter",
        "//Core/Service/Utility:algorithm_utils",
        "@mantle_api",
    ],
)

cc_test(
    name = "controller_prototypes_test",
    timeout = "short",
    srcs = ["controller_prototypes_test.cpp"],
    deps = [
        ":controller_prototypes",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "coordinate_converter",
    srcs = ["coordinate_converter.cpp"],
    hdrs = ["coordinate_converter.h"],
    visibility = ["//Core/Environment/GtGenEnvironment:__subpackages__"],
    deps = [
        "//Core/Environment/Map/Common:coordinate_converter",
        "@mantle_api",
    ],
)

cc_test(
    name = "coordinate_converter_test",
    timeout = "short",
    srcs = ["coordinate_converter_test.cpp"],
    deps = [
        ":coordinate_converter",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:mock_coordinate_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "entity_producer",
    srcs = ["entity_producer.cpp"],
    hdrs = ["entity_producer.h"],
    visibility = ["//Core/Environment/GtGenEnvironment:__subpackages__"],
    deps = [
        "//Core/Environment/Entities:entities",
        "//Core/Environment/StaticObjects:road_marking_properties_provider",
        "//Core/Environment/StaticObjects:static_object_properties_provider",
        "//Core/Environment/StaticObjects:supplementary_sign_properties_provider",
        "//Core/Environment/StaticObjects:traffic_light_properties_provider",
        "//Core/Environment/StaticObjects:traffic_sign_properties_provider",
        "//Core/Service/Utility:unique_id_provider",
        "@units_nhh",
    ],
)

cc_test(
    name = "entity_producer_test",
    srcs = ["entity_producer_test.cpp"],
    deps = [
        ":entity_producer",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "geometry_helper",
    srcs = ["geometry_helper.cpp"],
    hdrs = ["geometry_helper.h"],
    visibility = [
        "//Core/Environment/GtGenEnvironment:__subpackages__",
        "//Core/Environment/Map/LaneLocationProvider:__subpackages__",
    ],
    deps = [
        "//Core/Service/GlmWrapper:glm_wrapper",
        "@mantle_api",
    ],
)

cc_test(
    name = "geometry_helper_test",
    size = "small",
    srcs = ["geometry_helper_test.cpp"],
    deps = [
        ":geometry_helper",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "i_map_engine",
    hdrs = ["i_map_engine.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
    ],
    deps = [
        "//Core/Environment/Exception:exception",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Service/UserSettings:user_settings",
    ],
)

cc_library(
    name = "lane_assignment_service",
    srcs = ["lane_assignment_service.cpp"],
    hdrs = ["lane_assignment_service.h"],
    visibility = ["//Core/Environment/GtGenEnvironment:__subpackages__"],
    deps = [
        "//Core/Environment/GtGenEnvironment:entity_repository",
        "//Core/Environment/GtGenEnvironment/Internal:active_controller_repository",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Environment/Map/LaneLocationProvider:lane_location_provider",
        "//Core/Service/Utility:string_utils",
    ],
)

cc_test(
    name = "lane_assignment_service_test",
    timeout = "short",
    srcs = ["lane_assignment_service_test.cpp"],
    data = ["//Core/Environment/Controller/Internal/ControlUnits:traffic_participant_model_example.so"],
    deps = [
        ":lane_assignment_service",
        "//Core/Environment/Controller:composite_controller",
        "//Core/Environment/Controller:external_controller_config",
        "//Core/Environment/Controller/Internal/ControlUnits:i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/ControlUnits:traffic_participant_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Environment/GtGenEnvironment/Internal:active_controller_repository",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)
