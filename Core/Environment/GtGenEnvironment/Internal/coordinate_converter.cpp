/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/coordinate_converter.h"

#include <MantleAPI/Common/position.h>

namespace gtgen::core::environment::api
{
using units::literals::operator""_m;

mantle_api::Vec3<units::length::meter_t> CoordinateConverter::Convert(mantle_api::Position position)
{
    if (auto xyz_pos = std::get_if<mantle_api::Vec3<units::length::meter_t>>(&position))
    {
        return *xyz_pos;
    }

    UnderlyingMapCoordinate underlying_position;
    if (auto lane_pos = std::get_if<mantle_api::OpenDriveLanePosition>(&position))
    {
        underlying_position = *lane_pos;
    }
    else if (auto road_pos = std::get_if<mantle_api::OpenDriveRoadPosition>(&position))
    {
        underlying_position = *road_pos;
    }
    else if (auto lat_lon_pos = std::get_if<mantle_api::LatLonPosition>(&position))
    {
        underlying_position = *lat_lon_pos;
    }
    auto ret = underlying_converter_->Convert(underlying_position);

    if (ret.has_value())
    {
        auto value = ret.value();
        return {value.x, value.y, value.z};
    }
    return {-1_m, -1_m, -1_m};
}

mantle_api::Orientation3<units::angle::radian_t> CoordinateConverter::GetLaneOrientation(
    const mantle_api::OpenDriveLanePosition& open_drive_lane_position)
{
    return underlying_converter_->GetLaneOrientation(open_drive_lane_position);
}

mantle_api::Orientation3<units::angle::radian_t> CoordinateConverter::GetRoadOrientation(
    const mantle_api::OpenDriveRoadPosition& open_drive_road_position)
{
    return underlying_converter_->GetRoadOrientation(open_drive_road_position);
}
}  // namespace gtgen::core::environment::api
