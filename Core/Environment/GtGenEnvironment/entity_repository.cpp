/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/entity_repository.h"

#include "Core/Environment/Entities/traffic_sign_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Utility/algorithm_utils.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <algorithm>
#include <memory>

namespace gtgen::core::environment::api
{
const std::string kMountedToString = "mounted_to";

EntityRepository::EntityRepository(service::utility::UniqueIdProvider* unique_id_provider)
    : unique_id_provider_(unique_id_provider)
{
}

mantle_api::IVehicle& EntityRepository::Create(const std::string& name, const mantle_api::VehicleProperties& properties)
{
    mantle_api::UniqueId unique_id{0};
    if (!properties.is_host)
    {
        unique_id = CreateUniqueIdFromName(name);
    }

    return Create(unique_id, name, properties);
}

mantle_api::IVehicle& EntityRepository::Create(mantle_api::UniqueId id,
                                               const std::string& name,
                                               const mantle_api::VehicleProperties& properties)
{
    ThrowIfEntityWithIdExists(id);
    ThrowIfEntityWithNameExists(name);
    auto vehicle = entity_producer_.Produce(id, name, properties);
    if (properties.is_host)
    {
        host_ = vehicle.get();
    }
    entities_.push_back(std::move(vehicle));
    return dynamic_cast<mantle_api::IVehicle&>(*entities_.back());
}

mantle_api::IPedestrian& EntityRepository::Create(const std::string& name,
                                                  const mantle_api::PedestrianProperties& properties)
{
    return Create(CreateUniqueIdFromName(name), name, properties);
}

mantle_api::IPedestrian& EntityRepository::Create(mantle_api::UniqueId id,
                                                  const std::string& name,
                                                  const mantle_api::PedestrianProperties& properties)
{
    ThrowIfEntityWithIdExists(id);
    ThrowIfEntityWithNameExists(name);
    entities_.push_back(entity_producer_.Produce(id, name, properties));
    return dynamic_cast<mantle_api::IPedestrian&>(*entities_.back());
}

mantle_api::IStaticObject& EntityRepository::Create(const std::string& name,
                                                    const mantle_api::StaticObjectProperties& properties)
{
    return Create(CreateUniqueIdFromName(name), name, properties);
}

mantle_api::IStaticObject& EntityRepository::Create(mantle_api::UniqueId id,
                                                    const std::string& name,
                                                    const mantle_api::StaticObjectProperties& properties)
{
    ThrowIfEntityWithIdExists(id);
    ThrowIfEntityWithNameExists(name);

    auto entity = entity_producer_.Produce(id, name, properties);

    if (dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties()))
    {
        HandleTrafficSignEntity(id, entity.get());
    }
    else if (dynamic_cast<mantle_ext::SupplementarySignProperties*>(entity->GetProperties()))
    {
        HandleSupplementarySignEntity(id, properties, entity.get());
    }

    entities_.push_back(std::move(entity));
    return dynamic_cast<mantle_api::IStaticObject&>(*entities_.back());
}

mantle_api::IVehicle& EntityRepository::GetHost()
{
    if (host_ == nullptr)
    {
        Debug("Cannot return host. Host must be created first, make sure the is_host flag is set to true");
        throw EnvironmentException("Host could not be found. Please contact GTGen Support for further assistance.");
    }
    return dynamic_cast<mantle_api::IVehicle&>(*host_);
}

std::optional<std::reference_wrapper<mantle_api::IEntity>> EntityRepository::Get(const std::string& name)
{
    auto* entity_ptr = Find(name);
    if (entity_ptr == nullptr)
    {
        Debug("Could not find entity named '{}' in the entity repository. The entity must be created beforehand.",
              name);
        throw EnvironmentException(
            "Entity named '{}' could not be found. Please contact GTGen Support for further assistance.", name);
    }
    return *entity_ptr;
}

std::optional<std::reference_wrapper<const mantle_api::IEntity>> EntityRepository::Get(const std::string& name) const
{
    auto* entity_ptr = Find(name);
    if (entity_ptr == nullptr)
    {
        Debug("Could not find entity named '{}' in the entity repository. The entity must be created beforehand.",
              name);
        throw EnvironmentException(
            "Entity named '{}' could not be found. Please contact GTGen Support for further assistance.", name);
    }
    return *entity_ptr;
}

std::optional<std::reference_wrapper<mantle_api::IEntity>> EntityRepository::Get(mantle_api::UniqueId id)
{
    auto* entity_ptr = Find(id);
    if (entity_ptr == nullptr)
    {
        Debug("Could not find entity for ID {} in the entity repository. The entity must be created beforehand.", id);
        throw EnvironmentException(
            "Entity with ID {} could not be found. Please contact GTGen Support for further assistance.", id);
    }
    return *entity_ptr;
}

std::optional<std::reference_wrapper<const mantle_api::IEntity>> EntityRepository::Get(mantle_api::UniqueId id) const
{
    auto* entity_ptr = Find(id);
    if (entity_ptr == nullptr)
    {
        Debug("Could not find entity for ID {} in the entity repository. The entity must be created beforehand.", id);
        throw EnvironmentException(
            "Entity with ID {} could not be found. Please contact GTGen Support for further assistance.", id);
    }
    return *entity_ptr;
}

const std::vector<std::unique_ptr<mantle_api::IEntity>>& EntityRepository::GetEntities() const
{
    return entities_;
}

bool EntityRepository::Contains(mantle_api::UniqueId id) const
{
    return Find(id) != nullptr;
}

void EntityRepository::Delete(const std::string& name)
{
    (void)name;
}

void EntityRepository::Delete(mantle_api::UniqueId id)
{
    auto it = service::utility::FindObjectById(entities_, id);

    if (it == entities_.end())
    {
        throw EnvironmentException(
            "Tried to delete entity with ID {}, but it could not be found. Please contact GTGen Support for further "
            "assistance.",
            id);
    }

    Info("Deleting entity \"{}\" (id: {})", (*it)->GetName(), id);
    entities_.erase(it);
}

void EntityRepository::ThrowIfEntityWithIdExists(mantle_api::UniqueId id) const
{
    if (Find(id) != nullptr)
    {
        throw EnvironmentException(
            "An entity with ID {} already exists. If the scenario does not contain duplicate IDs, please contact GTGen "
            "Support for further assistance",
            id);
    }
}

void EntityRepository::ThrowIfEntityWithNameExists(const std::string& name) const
{
    if (Find(name) != nullptr)
    {
        throw EnvironmentException(
            "An entity with name '{}' already exists. If the scenario does not contain duplicate names, please contact "
            "GTGen Support for further assistance",
            name);
    }
}

mantle_api::IEntity* EntityRepository::Find(const std::string& name) const
{
    auto it =
        std::find_if(entities_.cbegin(), entities_.cend(), [&name](const std::unique_ptr<mantle_api::IEntity>& entity) {
            return entity->GetName() == name;
        });
    if (it != entities_.end())
    {
        return (*it).get();
    }
    return nullptr;
}

mantle_api::IEntity* EntityRepository::Find(mantle_api::UniqueId id) const
{
    auto it = service::utility::FindObjectById(entities_, id);
    if (it != entities_.end())
    {
        return (*it).get();
    }
    return nullptr;
}

mantle_api::UniqueId EntityRepository::CreateUniqueIdFromName(const std::string& name)
{
    if (first_id_creation_)
    {
        // Reset provider on first entity creation after road network has been created
        // At the moment it's sufficient if IDs are unique for entities
        ResetUniqueIdProviderForEntities();
        first_id_creation_ = false;
    }

    mantle_api::UniqueId unique_id{0};

    try
    {
        // If name is an unsigned integer between 1 and 1000, use this as id
        unique_id = std::stoull(name);
        if (unique_id == 0 || unique_id > 1000)
        {
            throw std::runtime_error("Name of entity '" + name +
                                     "' cannot be converted to ID. Only names with integer IDs in range [1;1000] "
                                     "allowed. Please adjust scenario.");
        }
    }
    catch (const std::logic_error&)
    {
        unique_id = unique_id_provider_->GetUniqueId();
    }

    return unique_id;
}

void EntityRepository::ResetUniqueIdProviderForEntities()
{
    unique_id_provider_->Reset();
    // IDs from 1 to 1000 are reserved for entities with names which are an integer
    unique_id_provider_->ReserveIds(1, 1000);
}

void EntityRepository::HandleTrafficSignEntity(const mantle_api::UniqueId& id, mantle_api::IEntity* entity)
{
    auto* traffic_sign_entity = dynamic_cast<entities::TrafficSignEntity*>(entity);
    if (!traffic_sign_entity)
    {
        return;
    }

    const auto& entity_name = entity->GetName();
    main_sign_name_to_mantle_id_.emplace(entity_name, id);

    auto range = supp_sign_reference_name_to_mantle_id_.equal_range(entity_name);

    for (auto it = range.first; it != range.second; ++it)
    {
        if (auto supp_sign = Find(it->second))
        {
            traffic_sign_entity->AddSupplementarySign(it->second, *supp_sign);
        }
    }

    supp_sign_reference_name_to_mantle_id_.erase(entity_name);
}

void EntityRepository::HandleSupplementarySignEntity(const mantle_api::UniqueId& id,
                                                     const mantle_api::StaticObjectProperties& properties,
                                                     mantle_api::IEntity* entity)
{
    auto main_sign_reference_name_it = properties.properties.find(kMountedToString);
    if (main_sign_reference_name_it == properties.properties.end())
        return;

    const auto& main_sign_reference_name = main_sign_reference_name_it->second;
    auto main_sign_mantle_id_it = main_sign_name_to_mantle_id_.find(main_sign_reference_name);

    if (main_sign_mantle_id_it == main_sign_name_to_mantle_id_.end())
    {
        supp_sign_reference_name_to_mantle_id_.emplace(main_sign_reference_name, id);
        return;
    }

    auto main_sign_ptr = Find(main_sign_mantle_id_it->second);
    if (!main_sign_ptr)
    {
        return;
    }

    if (auto* traffic_sign_entity = dynamic_cast<entities::TrafficSignEntity*>(main_sign_ptr))
    {
        traffic_sign_entity->AddSupplementarySign(entity->GetUniqueId(), *entity);
    }
}

}  // namespace gtgen::core::environment::api
