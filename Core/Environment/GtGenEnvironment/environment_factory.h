/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_ENVIRONMENTFACTORY_H
#define GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_ENVIRONMENTFACTORY_H

#include "Core/Environment/GtGenEnvironment/Internal/i_map_engine.h"
#include "Core/Environment/GtGenEnvironment/gtgen_environment.h"
#include "Core/Service/UserSettings/user_settings.h"

#include <MantleAPI/Execution/i_environment.h>

namespace gtgen::core::environment::api
{

class EnvironmentFactory
{
  public:
    static std::unique_ptr<environment::api::GtGenEnvironment> Create(
        const service::user_settings::UserSettings& user_settings,
        const mantle_api::Time step_size,
        const std::uint32_t seed = 0U);
};

}  // namespace gtgen::core::environment::api
#endif  // GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_ENVIRONMENTFACTORY_H
