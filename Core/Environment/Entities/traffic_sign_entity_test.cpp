/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/traffic_sign_entity.h"

#include <gtest/gtest.h>

#include <memory>

namespace gtgen::core::environment::entities
{
namespace
{
using units::literals::operator""_m;
using units::literals::operator""_rad;

class TrafficSignEntityTest : public ::testing::Test
{
  protected:
    void SetUp() override { traffic_sign = std::make_unique<TrafficSignEntity>(1, "MainSign"); }

    std::unique_ptr<TrafficSignEntity> traffic_sign;

    std::unique_ptr<StaticObject> CreateAndSetupSupplementarySign(
        mantle_api::UniqueId id,
        units::length::meter_t vertical_offset,
        const mantle_api::Vec3<units::length::meter_t>& geometric_center,
        const std::optional<mantle_api::Vec3<units::length::meter_t>>& position = std::nullopt,
        const std::optional<mantle_api::Orientation3<units::angle::radian_t>>& orientation = std::nullopt)
    {
        auto supplementary_sign = std::make_unique<StaticObject>(id, "SupplementarySign");
        auto supplementary_properties = std::make_unique<mantle_api::StaticObjectProperties>();
        supplementary_properties->vertical_offset = vertical_offset;
        supplementary_properties->bounding_box.geometric_center = geometric_center;
        supplementary_sign->SetProperties(std::move(supplementary_properties));

        if (orientation)
        {
            supplementary_sign->SetOrientation(*orientation);
        }

        if (position)
        {
            supplementary_sign->SetPosition(*position);
        }

        return supplementary_sign;
    }

    std::unique_ptr<TrafficSignEntity> CreateAndSetUpTrafficSign(
        mantle_api::UniqueId id,
        units::length::meter_t vertical_offset,
        const mantle_api::Vec3<units::length::meter_t>& geometric_center)
    {
        auto traffic_sign = std::make_unique<TrafficSignEntity>(id, "MainSign");
        auto main_sign_properties = std::make_unique<mantle_api::StaticObjectProperties>();
        main_sign_properties->vertical_offset = vertical_offset;
        main_sign_properties->bounding_box.geometric_center = geometric_center;
        traffic_sign->SetProperties(std::move(main_sign_properties));
        return traffic_sign;
    }

    void SetMainSignOrientationAndPosition(TrafficSignEntity& traffic_sign,
                                           const mantle_api::Orientation3<units::angle::radian_t>& orientation,
                                           const mantle_api::Vec3<units::length::meter_t>& position)
    {
        traffic_sign.SetOrientation(orientation);
        traffic_sign.SetPosition(position);
    }
};

TEST_F(TrafficSignEntityTest, GivenSupplementarySign_WhenAddedToMainSign_ThenAddedCorrectly)
{
    mantle_api::UniqueId supplementary_id = 2;
    auto supplementary_sign = std::make_unique<StaticObject>(supplementary_id, "SupplementarySign");

    traffic_sign->AddSupplementarySign(supplementary_id, *supplementary_sign);

    auto retrieved_sign = traffic_sign->GetSupplementarySign(supplementary_id);
    ASSERT_NE(retrieved_sign, nullptr);
    EXPECT_EQ(retrieved_sign->GetName(), "SupplementarySign");
}

TEST_F(TrafficSignEntityTest, GivenTwoSupplementarySigns_WhenAddedToMainSign_ThenAddedCorrectly)
{
    mantle_api::UniqueId id1 = 2, id2 = 3;
    auto sign1 = std::make_unique<StaticObject>(id1, "Sign1");
    auto sign2 = std::make_unique<StaticObject>(id2, "Sign2");

    traffic_sign->AddSupplementarySign(id1, *sign1);
    traffic_sign->AddSupplementarySign(id2, *sign2);

    auto supplementary_signs = traffic_sign->GetSupplementarySigns();
    EXPECT_EQ(supplementary_signs.size(), 2);
    EXPECT_NE(supplementary_signs.find(id1), supplementary_signs.end());
    EXPECT_NE(supplementary_signs.find(id2), supplementary_signs.end());
}

TEST_F(TrafficSignEntityTest, GivenNonExistentSupplementarySignId_WhenGetSupplementarySign_ThenReturnNull)
{
    mantle_api::UniqueId non_existent_id = 999;
    auto retrieved_sign = traffic_sign->GetSupplementarySign(non_existent_id);
    EXPECT_EQ(retrieved_sign, nullptr);
}

TEST_F(TrafficSignEntityTest, GivenValidSupplementarySignId_WhenDelete_ThenDeletedCorrectly)
{
    mantle_api::UniqueId id = 2;
    auto sign = std::make_unique<StaticObject>(id, "Sign");

    traffic_sign->AddSupplementarySign(id, *sign);
    EXPECT_TRUE(traffic_sign->DeleteSupplementarySign(id));
    EXPECT_EQ(traffic_sign->GetSupplementarySign(id), nullptr);
}

TEST_F(TrafficSignEntityTest, GivenNonExistentSupplementarySignId_WhenDelete_ThenReturnFalse)
{
    mantle_api::UniqueId non_existent_id = 999;
    EXPECT_FALSE(traffic_sign->DeleteSupplementarySign(non_existent_id));
}

TEST_F(TrafficSignEntityTest,
       GivenMainSignWithUninitializedOrientation_WhenSetPosition_ThenSupplementarySignPositionIsNotOverridden)
{
    const auto expected_supplementary_position = mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m};
    auto supplementary_sign =
        CreateAndSetupSupplementarySign(2, 1.5_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});

    auto traffic_sign = CreateAndSetUpTrafficSign(1, 2.2_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});
    traffic_sign->AddSupplementarySign(2, *supplementary_sign);
    traffic_sign->SetPosition(mantle_api::Vec3<units::length::meter_t>{2_m, 3_m, 2.2_m});

    EXPECT_DOUBLE_EQ(expected_supplementary_position.x.value(), supplementary_sign->GetPosition().x.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_position.y.value(), supplementary_sign->GetPosition().y.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_position.z.value(), supplementary_sign->GetPosition().z.value());
}

TEST_F(
    TrafficSignEntityTest,
    GivenMainSignWithPositionalInitializedSupplementarySign_WhenSetPosition_ThenSupplementarySignPositionIsNotOverridden)
{
    const auto expected_supplementary_position = mantle_api::Vec3<units::length::meter_t>{1_m, 1_m, 1.6_m};
    auto supplementary_sign = CreateAndSetupSupplementarySign(
        2, 1.5_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m}, expected_supplementary_position);

    auto traffic_sign = CreateAndSetUpTrafficSign(1, 2.2_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});
    traffic_sign->AddSupplementarySign(2, *supplementary_sign);
    traffic_sign->SetOrientation(mantle_api::Orientation3<units::angle::radian_t>{3.1415_rad, 0_rad, 0_rad});
    traffic_sign->SetPosition(mantle_api::Vec3<units::length::meter_t>{2_m, 3_m, 2.2_m});

    EXPECT_DOUBLE_EQ(expected_supplementary_position.x.value(), supplementary_sign->GetPosition().x.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_position.y.value(), supplementary_sign->GetPosition().y.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_position.z.value(), supplementary_sign->GetPosition().z.value());
}

TEST_F(TrafficSignEntityTest,
       GivenMainSignWithYawAngleAndSupplementarySign_WhenSetPosition_ThenSupplementarySignPositionMatchesExpected)
{
    const auto expected_supplementary_position = mantle_api::Vec3<units::length::meter_t>{2_m, 3_m, 1.6_m};
    auto supplementary_sign =
        CreateAndSetupSupplementarySign(2, 1.5_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});

    auto traffic_sign = CreateAndSetUpTrafficSign(1, 2.2_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});
    traffic_sign->AddSupplementarySign(2, *supplementary_sign);
    SetMainSignOrientationAndPosition(*traffic_sign,
                                      mantle_api::Orientation3<units::angle::radian_t>{3.1415_rad, 0_rad, 0_rad},
                                      mantle_api::Vec3<units::length::meter_t>{2_m, 3_m, 2.3_m});

    EXPECT_DOUBLE_EQ(expected_supplementary_position.x.value(), supplementary_sign->GetPosition().x.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_position.y.value(), supplementary_sign->GetPosition().y.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_position.z.value(), supplementary_sign->GetPosition().z.value());
}

TEST_F(TrafficSignEntityTest,
       GivenMainSignWithPitchAngleAndSupplementarySign_WhenSetPosition_ThenSupplementarySignPositionMatchesExpected)
{
    const auto expected_supplementary_position = mantle_api::Vec3<units::length::meter_t>{1.293_m, 3.0_m, 3.293_m};
    auto supplementary_sign =
        CreateAndSetupSupplementarySign(2, 1.0_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});

    auto traffic_sign = CreateAndSetUpTrafficSign(1, 2.0_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});
    traffic_sign->AddSupplementarySign(2, *supplementary_sign);
    SetMainSignOrientationAndPosition(*traffic_sign,
                                      mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0.785_rad, 0_rad},
                                      mantle_api::Vec3<units::length::meter_t>{2.0_m, 3.0_m, 4.0_m});

    EXPECT_NEAR(expected_supplementary_position.x.value(), supplementary_sign->GetPosition().x.value(), 1e-3);
    EXPECT_NEAR(expected_supplementary_position.y.value(), supplementary_sign->GetPosition().y.value(), 1e-3);
    EXPECT_NEAR(expected_supplementary_position.z.value(), supplementary_sign->GetPosition().z.value(), 1e-3);
}

TEST_F(
    TrafficSignEntityTest,
    GivenMainSignAndSupplementarySignWithInitializedOrientation_WhenSetOrientation_ThenSupplementarySignOrientationIsNotOverridden)
{
    const auto expected_supplementary_orientation =
        mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 1_rad};
    auto supplementary_sign = CreateAndSetupSupplementarySign(2,
                                                              1.5_m,
                                                              mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m},
                                                              std::nullopt,
                                                              expected_supplementary_orientation);

    auto traffic_sign = CreateAndSetUpTrafficSign(1, 2.2_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});
    traffic_sign->AddSupplementarySign(2, *supplementary_sign);
    traffic_sign->SetOrientation(mantle_api::Orientation3<units::angle::radian_t>{1_rad, 1_rad, 1_rad});

    EXPECT_DOUBLE_EQ(expected_supplementary_orientation.yaw.value(), supplementary_sign->GetOrientation().yaw.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_orientation.pitch.value(),
                     supplementary_sign->GetOrientation().pitch.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_orientation.roll.value(),
                     supplementary_sign->GetOrientation().roll.value());
}

TEST_F(
    TrafficSignEntityTest,
    GivenMainSignAndSupplementarySignWithUninitializedOrientation_WhenSetOrientation_ThenSupplementarySignOrientationIOverridden)
{
    const auto expected_supplementary_orientation =
        mantle_api::Orientation3<units::angle::radian_t>{1_rad, 1_rad, 1_rad};
    auto supplementary_sign =
        CreateAndSetupSupplementarySign(2, 1.5_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});

    auto traffic_sign = CreateAndSetUpTrafficSign(1, 2.2_m, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0.1_m});
    traffic_sign->AddSupplementarySign(2, *supplementary_sign);
    traffic_sign->SetOrientation(mantle_api::Orientation3<units::angle::radian_t>{1_rad, 1_rad, 1_rad});

    EXPECT_DOUBLE_EQ(expected_supplementary_orientation.yaw.value(), supplementary_sign->GetOrientation().yaw.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_orientation.pitch.value(),
                     supplementary_sign->GetOrientation().pitch.value());
    EXPECT_DOUBLE_EQ(expected_supplementary_orientation.roll.value(),
                     supplementary_sign->GetOrientation().roll.value());
}

}  // namespace
}  // namespace gtgen::core::environment::entities
