/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/static_object_entity.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::entities
{
namespace
{
using units::literals::operator""_m;
using units::literals::operator""_rad;

class StaticObjectEntityTest : public ::testing::Test
{
  protected:
    mantle_api::UniqueId test_id{1};
    std::string test_name{"TestObject"};
    gtgen::core::environment::entities::StaticObject static_object{test_id, test_name};
};

TEST_F(StaticObjectEntityTest, GivenNewStaticObject_WhenCreated_ThenPositionAndOrientationAreNotSet)
{
    EXPECT_FALSE(static_object.IsPositionSet());
    EXPECT_FALSE(static_object.IsOrientationSet());
}

TEST_F(StaticObjectEntityTest, GivenStaticObject_WhenSetPositionCalled_ThenPositionIsSet)
{
    mantle_api::Vec3<units::length::meter_t> position{1.0_m, 2.0_m, 3.0_m};
    static_object.SetPosition(position);

    EXPECT_TRUE(static_object.IsPositionSet());
}

TEST_F(StaticObjectEntityTest, GivenStaticObject_WhenSetOrientationCalled_ThenOrientationIsSet)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation{0.1_rad, 0.2_rad, 0.3_rad};
    static_object.SetOrientation(orientation);

    EXPECT_TRUE(static_object.IsOrientationSet());
}

}  // namespace
}  // namespace gtgen::core::environment::entities
