/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_STATICOBJECTENTITY_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_STATICOBJECTENTITY_H

#include "Core/Environment/Entities/Internal/base_entity.h"

namespace gtgen::core::environment::entities
{
class StaticObject : public BaseEntity, public mantle_api::IStaticObject
{
  public:
    StaticObject(mantle_api::UniqueId id, const std::string& name) : BaseEntity(id, name) {}

    mantle_api::StaticObjectProperties* GetProperties() const override;
    void SetPosition(const mantle_api::Vec3<units::length::meter_t>& position) override;
    void SetOrientation(const mantle_api::Orientation3<units::angle::radian_t>& orientation) override;
    bool IsPositionSet() const;
    bool IsOrientationSet() const;

  protected:
    bool position_set_{false};
    bool orientation_set_{false};
};
}  // namespace gtgen::core::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_STATICOBJECTENTITY_H
