/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/road_object_converter.h"

#include "Core/Environment/Map/MapApiConverter/Internal/road_object_types_converter.h"

namespace gtgen::core::environment::map
{

RoadObject ConvertRoadObject(const map_api::StationaryObject& from)
{
    RoadObject result;

    result.base_polygon = from.base.base_polygon;
    result.pose = mantle_api::Pose{from.base.position, from.base.orientation};
    result.dimensions = from.base.dimension;
    result.type = ConvertRoadObjectType(from.type);
    result.material = ConvertRoadObjectMaterial(from.material);
    result.id = from.id;

    return result;
}

}  // namespace gtgen::core::environment::map
