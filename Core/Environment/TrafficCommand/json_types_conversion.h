/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_JSONTYPESCONVERSION_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_JSONTYPESCONVERSION_H

#include <MantleAPI/Traffic/control_strategy.h>
#include <nlohmann/json.hpp>

#include <string>

namespace mantle_api
{

NLOHMANN_JSON_SERIALIZE_ENUM(VehicleLightType,  // NOLINT(cppcoreguidelines-avoid-c-arrays)
                             {{VehicleLightType::kBrakeLights, "brakeLights"},
                              {VehicleLightType::kDaytimeRunningLights, "daytimeRunningLights"},
                              {VehicleLightType::kFogLights, "fogLights"},
                              {VehicleLightType::kFogLightsFront, "fogLightsFront"},
                              {VehicleLightType::kFogLightsRear, "fogLightsRear"},
                              {VehicleLightType::kHighBeam, "highBeam"},
                              {VehicleLightType::kIndicatorLeft, "indicatorLeft"},
                              {VehicleLightType::kIndicatorRight, "indicatorRight"},
                              {VehicleLightType::kLicensePlateIllumination, "licensePlateIllumination"},
                              {VehicleLightType::kLowBeam, "lowBeam"},
                              {VehicleLightType::kReversingLights, "reversingLights"},
                              {VehicleLightType::kSpecialPurposeLights, "specialPurposeLights"},
                              {VehicleLightType::kWarningLights, "warningLights"}})

NLOHMANN_JSON_SERIALIZE_ENUM(LightMode,  // NOLINT(cppcoreguidelines-avoid-c-arrays)
                             {{LightMode::kFlashing, "flashing"}, {LightMode::kOff, "off"}, {LightMode::kOn, "on"}})

// NOLINTNEXTLINE(readability-identifier-naming)
inline void to_json(nlohmann::ordered_json& node, const LightType& light_type)
{
    if (const VehicleLightType* vehicle_light_type = std::get_if<VehicleLightType>(&light_type))
    {
        node["vehicleLight"] = *vehicle_light_type;
    }
    else
    {
        node["userDefinedLight"] = std::get<std::string>(light_type);
    }
}

// NOLINTNEXTLINE(readability-identifier-naming)
inline void to_json(nlohmann::ordered_json& node, const LightState& light_state)
{
    node["mode"] = light_state.light_mode;
}

// NOLINTNEXTLINE(readability-identifier-naming)
inline void to_json(nlohmann::ordered_json& node,
                    const VehicleLightStatesControlStrategy& vehicle_light_states_control_strategy)
{
    node["lightType"] = vehicle_light_states_control_strategy.light_type;
    node["lightState"] = vehicle_light_states_control_strategy.light_state;
}

}  // namespace mantle_api
#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_JSONTYPESCONVERSION_H
