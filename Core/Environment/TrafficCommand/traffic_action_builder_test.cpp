/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_action_builder.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::traffic_command
{
using units::literals::operator""_mps;
using units::literals::operator""_m;
using units::literals::operator""_rad;

class TrafficActionBuilderTest : public testing::Test
{
  protected:
    entities::VehicleEntity vehicle_entity_{0, "host"};
    std::unique_ptr<map::GtGenMap> gtgen_map_{test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints()};
    map::LaneLocationProvider lane_location_provider_{*gtgen_map_};
};

TEST_F(TrafficActionBuilderTest, GivenUnsupportedControlStrategy_WhenCallConvertToTrafficAction_ThenNoValueReturned)
{
    mantle_api::KeepVelocityControlStrategy control_strategy;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    EXPECT_FALSE(result.has_value());
}

TEST_F(TrafficActionBuilderTest,
       GivenFollowVelocitySplineControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithSpeedActionReturned)
{
    mantle_api::FollowVelocitySplineControlStrategy control_strategy;
    control_strategy.default_value = 10_mps;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_speed_action());
    EXPECT_EQ(control_strategy.default_value(), result.value().speed_action().absolute_target_speed());
}

TEST_F(
    TrafficActionBuilderTest,
    GivenPerformLaneChangeControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithLaneChangeActionReturned)
{
    mantle_api::PerformLaneChangeControlStrategy control_strategy;
    control_strategy.target_lane_id = -3;  // shift 1 lane right

    vehicle_entity_.SetPosition(mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m});  // on local lane -2

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_lane_change_action());
    EXPECT_EQ(1, result.value().lane_change_action().relative_target_lane());
}

TEST_F(
    TrafficActionBuilderTest,
    GivenAcquireLaneOffsetControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithLaneOffsetActionReturned)
{
    mantle_api::AcquireLaneOffsetControlStrategy control_strategy;
    control_strategy.offset = 1_m;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_lane_offset_action());
    EXPECT_EQ(control_strategy.offset(), result.value().lane_offset_action().target_lane_offset());
}

TEST_F(TrafficActionBuilderTest,
       GivenFollowTrajectoryControlStrategyWithTimeReference_WhenCallConvertToTrafficAction_ThenNoValueReturned)
{
    mantle_api::FollowTrajectoryControlStrategy control_strategy;
    mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference;
    control_strategy.timeReference = time_reference;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_FALSE(result.has_value());
}

TEST_F(TrafficActionBuilderTest,
       GivenFollowTrajectoryControlStrategyWithoutPolyline_WhenCallConvertToTrafficAction_ThenThrow)
{
    mantle_api::FollowTrajectoryControlStrategy control_strategy;

    EXPECT_ANY_THROW(
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy));
}

TEST_F(TrafficActionBuilderTest,
       GivenFollowTrajectoryControlStrategyWithoutPoints_WhenCallConvertToTrafficAction_ThenThrow)
{
    mantle_api::FollowTrajectoryControlStrategy control_strategy;
    mantle_api::PolyLine polyline;
    control_strategy.trajectory.type = polyline;

    EXPECT_ANY_THROW(
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy));
}

TEST_F(TrafficActionBuilderTest,
       GivenFollowTrajectoryControlStrategyWithOnePoint_WhenCallConvertToTrafficAction_ThenThrow)
{
    mantle_api::FollowTrajectoryControlStrategy control_strategy;
    mantle_api::PolyLine polyline;
    auto point1 = mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m};
    polyline.push_back({{point1, {0_rad, 0_rad, 0_rad}}, {}});
    control_strategy.trajectory.type = polyline;

    EXPECT_ANY_THROW(
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy));
}

TEST_F(
    TrafficActionBuilderTest,
    GivenFollowTrajectoryControlStrategyWithoutTimeWithPoints_WhenCallConvertToTrafficAction_ThenTrafficActionWithFollowPathActionReturned)
{
    mantle_api::FollowTrajectoryControlStrategy control_strategy;
    mantle_api::PolyLine polyline;
    auto point1 = mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m};
    auto point2 = mantle_api::Vec3<units::length::meter_t>{4_m, 5_m, 6_m};
    polyline.push_back({{point1, {0_rad, 0_rad, 0_rad}}, {}});
    polyline.push_back({{point2, {0_rad, 0_rad, 0_rad}}, {}});
    control_strategy.trajectory.type = polyline;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_follow_path_action());
    ASSERT_EQ(result.value().follow_path_action().path_point().size(), 2);
    EXPECT_TRIPLE(result.value().follow_path_action().path_point()[0].position(), point1);
    EXPECT_TRIPLE(result.value().follow_path_action().path_point()[1].position(), point2);
}

TEST_F(TrafficActionBuilderTest,
       GivenVehicleLightStatesControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    mantle_api::VehicleLightStatesControlStrategy control_strategy;
    control_strategy.light_type = mantle_api::VehicleLightType::kIndicatorLeft;
    control_strategy.light_state = {mantle_api::LightMode::kFlashing};

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    std::string expected_command_type = "light_state_action";
    std::string expected_command =
        "{\"lightType\":{\"vehicleLight\":\"indicatorLeft\"},\"lightState\":{\"mode\":\"flashing\"}}";

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_custom_action());
    EXPECT_EQ(expected_command_type, result.value().custom_action().command_type());
    EXPECT_EQ(expected_command, result.value().custom_action().command());
}

TEST_F(TrafficActionBuilderTest,
       GivenCommandTypeAndCommand_WhenCallCreateCustomTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    std::string command_type = "route";
    std::string command = "5,6,7,8";
    auto result = TrafficActionBuilder::CreateCustomTrafficAction(command_type, command);

    ASSERT_TRUE(result.has_custom_action());
    EXPECT_EQ(command_type, result.custom_action().command_type());
    EXPECT_EQ(command, result.custom_action().command());
}

}  // namespace gtgen::core::environment::traffic_command
