/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MOCKMAPENGINE_MOCKMAPENGINE_H
#define GTGEN_CORE_TESTS_TESTUTILS_MOCKMAPENGINE_MOCKMAPENGINE_H

#include "Core/Environment/GtGenEnvironment/Internal/i_map_engine.h"

namespace gtgen::core::test_utils
{

class MockMapEngine : public environment::api::IMapEngine
{
  public:
    void SetMap(std::unique_ptr<environment::map::GtGenMap> map) { map_ = std::move(map); }

    void Load(const std::string&,
              const service::user_settings::UserSettings&,
              const mantle_api::MapDetails&,
              environment::map::GtGenMap& map,
              service::utility::UniqueIdProvider*) override
    {
        if (map_)
        {
            map = std::move(*map_);
        }
        else
        {
            throw std::runtime_error("No Map was set in MockMapEngine before calling Load()");
        }
    }

  private:
    std::unique_ptr<environment::map::GtGenMap> map_;
};

}  // namespace gtgen::core::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MOCKMAPENGINE_MOCKMAPENGINE_H
