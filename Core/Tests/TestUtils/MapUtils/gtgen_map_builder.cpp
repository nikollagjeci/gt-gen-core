/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Tests/TestUtils/MapUtils/gtgen_map_builder.h"

#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

namespace gtgen::core::test_utils
{

void GtGenMapBuilder::AddLane(const mantle_api::UniqueId lane_group_id,
                              const mantle_api::UniqueId lane_id,
                              const std::vector<mantle_api::Vec3<units::length::meter_t>>& centerline,
                              const std::vector<environment::map::LaneBoundary::Points>& right_boundary_points,
                              const std::vector<mantle_api::UniqueId>& successors,
                              const std::vector<mantle_api::UniqueId>& predecessors)
{
    if (gtgen_map.FindLaneGroup(lane_group_id) == nullptr)
    {
        gtgen_map.AddLaneGroup(environment::map::LaneGroup{lane_group_id, environment::map::LaneGroup::Type::kUnknown});
    }

    environment::map::Lane lane{lane_id};

    if (drivable_)
    {
        lane.flags.SetDrivable();
    }

    lane.center_line = centerline;

    for (std::size_t i{0}; i < right_boundary_points.size(); ++i)
    {
        const auto lane_boundary_id = lane_id + 1000 + i;

        auto boundary = environment::map::LaneBoundary{lane_boundary_id,
                                                       environment::map::LaneBoundary::Type::kSolidLine,
                                                       environment::map::LaneBoundary::Color::kWhite};
        boundary.points = right_boundary_points[i];

        gtgen_map.AddLaneBoundary(lane_group_id, boundary);
        lane.right_lane_boundaries.emplace_back(lane_boundary_id);
    }

    for (const auto& successor : successors)
    {
        lane.successors.push_back(successor);
    }

    for (const auto& predecessor : predecessors)
    {
        lane.predecessors.push_back(predecessor);
    }

    for (const auto& right_lanes : right_adjacent_lanes_)
    {
        lane.right_adjacent_lanes.push_back(right_lanes);
    }

    for (const auto& left_lanes : left_adjacent_lanes_)
    {
        lane.left_adjacent_lanes.push_back(left_lanes);
    }

    for (const auto& left_boundary : left_lane_boundaries_)
    {
        lane.left_lane_boundaries.push_back(left_boundary);
    }

    for (const auto& right_boundary : right_lane_boundaries_)
    {
        lane.right_lane_boundaries.push_back(right_boundary);
    }

    gtgen_map.AddLane(lane_group_id, lane);
}

void GtGenMapBuilder::SetIds(const mantle_api::UniqueId lane_group_id, const mantle_api::UniqueId lane_id)
{
    lane_id_ = lane_id;
    lane_group_id_ = lane_group_id;
}

void GtGenMapBuilder::SetDrivable(bool drivable)
{
    drivable_ = drivable;
}

void GtGenMapBuilder::SetCenterLine(const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line)
{
    center_line_ = center_line;
}
void GtGenMapBuilder::ClearCenterLine()
{
    center_line_.clear();
}

void GtGenMapBuilder::SetRightBoundaries(const std::vector<environment::map::LaneBoundary::Points>& right_boundaries)
{
    right_boundaries_ = right_boundaries;
}
void GtGenMapBuilder::AddRightBoundary(const environment::map::LaneBoundary::Points& right_boundary)
{
    right_boundaries_.emplace_back(right_boundary);
}
void GtGenMapBuilder::ClearRightBoundaries()
{
    right_boundaries_.clear();
}

void GtGenMapBuilder::SetSuccessors(const std::vector<mantle_api::UniqueId>& successors)
{
    successors_ = successors;
}
void GtGenMapBuilder::AddSuccessor(const mantle_api::UniqueId successor)
{
    successors_.emplace_back(successor);
}
void GtGenMapBuilder::ClearSuccessors()
{
    successors_.clear();
}

void GtGenMapBuilder::SetPredecessors(const std::vector<mantle_api::UniqueId>& predecessors)
{
    predecessors_ = predecessors;
}
void GtGenMapBuilder::AddPredecessor(const mantle_api::UniqueId predecessor)
{
    predecessors_.emplace_back(predecessor);
}
void GtGenMapBuilder::ClearPredecessors()
{
    predecessors_.clear();
}

void GtGenMapBuilder::SetLeftAdjacentLanes(const std::vector<mantle_api::UniqueId>& left_adjacent_lanes)
{
    left_adjacent_lanes_ = left_adjacent_lanes;
}
void GtGenMapBuilder::AddLeftAdjacentLane(const mantle_api::UniqueId left_adjacent_lane)
{
    left_adjacent_lanes_.push_back(left_adjacent_lane);
}
void GtGenMapBuilder::ClearLeftAdjacentLanes()
{
    left_adjacent_lanes_.clear();
}

void GtGenMapBuilder::SetRightAdjacentLanes(const std::vector<mantle_api::UniqueId>& right_adjacent_lanes)
{
    right_adjacent_lanes_ = right_adjacent_lanes;
}

void GtGenMapBuilder::AddRightAdjacentLane(const mantle_api::UniqueId right_adjacent_lane)
{
    right_adjacent_lanes_.push_back(right_adjacent_lane);
}
void GtGenMapBuilder::CleaRightAdjacentLanes()
{
    right_adjacent_lanes_.clear();
}

void GtGenMapBuilder::SetLeftLaneBoundaries(const std::vector<mantle_api::UniqueId>& left_lane_boundaries)
{
    left_lane_boundaries_ = left_lane_boundaries;
}
void GtGenMapBuilder::AddLeftLaneBoundaries(const mantle_api::UniqueId left_lane_boundaries)
{
    left_lane_boundaries_.push_back(left_lane_boundaries);
}
void GtGenMapBuilder::ClearLeftLaneBoundaries()
{
    left_lane_boundaries_.clear();
}

void GtGenMapBuilder::SetRightLaneBoundaries(const std::vector<mantle_api::UniqueId>& right_lane_boundaries)
{
    right_lane_boundaries_ = right_lane_boundaries;
}
void GtGenMapBuilder::AddRightLaneBoundaries(const mantle_api::UniqueId right_lane_boundaries)
{
    right_lane_boundaries_.push_back(right_lane_boundaries);
}

void GtGenMapBuilder::CleaRightLaneBoundaries()
{
    right_lane_boundaries_.clear();
}

void GtGenMapBuilder::CreateLane()
{
    AddLane(lane_group_id_, lane_id_, center_line_, right_boundaries_, successors_, predecessors_);
}

void GtGenMapBuilder::SetMapPath(std::string fake_path)
{
    gtgen_map.path = std::move(fake_path);
}

void GtGenMapBuilder::AddRoadObject(const environment::map::RoadObject& road_object)
{
    gtgen_map.road_objects.push_back(road_object);
}

void GtGenMapBuilder::AddTrafficSign(const std::shared_ptr<environment::map::TrafficSign>& sign)
{
    gtgen_map.traffic_signs.push_back(sign);
}

void GtGenMapBuilder::AddTrafficLight(const environment::map::TrafficLight& traffic_light)
{
    gtgen_map.traffic_lights.push_back(traffic_light);
}

void GtGenMapBuilder::AddPatch(const std::shared_ptr<environment::map::Patch>& patch)
{
    gtgen_map.patches.push_back(patch);
}

void GtGenMapBuilder::Clear()
{
    ClearCenterLine();
    ClearRightBoundaries();
    ClearSuccessors();
    ClearPredecessors();
    CleaRightAdjacentLanes();
    ClearLeftAdjacentLanes();
}

environment::map::Lane& GtGenMapBuilder::GetMutableLane(const mantle_api::UniqueId lane_id)
{
    return gtgen_map.GetLane(lane_id);
}

}  // namespace gtgen::core::test_utils
